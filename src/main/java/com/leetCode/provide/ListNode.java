package com.leetCode.provide;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class ListNode {

    private int val;

    private ListNode next;

    public ListNode() {
    }

    public ListNode(int val) {
        this.val = val;
    }

    public ListNode(int val, ListNode next) {
        this.val = val;
        this.next = next;
    }

    public ListNode(int...arr) {
        if (arr.length > 0) {
            this.val = arr[0];
            ListNode operation = this;
            for (int i = 1; i < arr.length; i++) {
                operation.setNext(new ListNode());
                operation = operation.getNext();
                operation.setVal(arr[i]);
            }
        }
    }

    @Override
    public String toString() {
        List<Integer> list = new ArrayList<>();
        ListNode listNode = this;
        while (listNode != null) {
            list.add(listNode.getVal());
            listNode = listNode.getNext();
        }

        return list.toString();
    }

}