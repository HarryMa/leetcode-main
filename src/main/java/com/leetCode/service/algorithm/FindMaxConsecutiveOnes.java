package com.leetCode.service.algorithm;

public interface FindMaxConsecutiveOnes {

    /**
     * 最大连续 1 的个数
     * 给定一个二进制数组， 计算其中最大连续 1 的个数。
     *
     * @param nums 只包含0和1, 给定一个二进制数组， 计算其中最大连续 1 的个数。
     * @return 最大连续1的个数
     */
    int findMaxConsecutiveOnes(int[] nums);

}
