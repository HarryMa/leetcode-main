package com.leetCode.service.algorithm;

public interface ThirdMax {

    /**
     * 第三大的数
     * 给你一个非空数组，返回此数组中 第三大的数。如果不存在，则返回数组中最大的数。
     *
     * @param nums 1 <= nums.length <= 10^4, -2^31 <= nums[i] <= 2^31 - 1
     * @return 第三大的数
     */
    int thirdMax(int[] nums);

}
