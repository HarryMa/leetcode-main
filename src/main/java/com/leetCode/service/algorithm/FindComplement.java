package com.leetCode.service.algorithm;

public interface FindComplement {

    /**
     * 数字的补数
     * 对整数的二进制表示取反（0 变 1 ，1 变 0）后，再转换为十进制表示，可以得到这个整数的补数。
     *
     * @param num 1 <= num < 2^31
     * @return 补数
     */
    int findComplement(int num);

}
