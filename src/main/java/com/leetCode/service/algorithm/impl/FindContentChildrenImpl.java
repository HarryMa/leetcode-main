package com.leetCode.service.algorithm.impl;

import com.leetCode.service.algorithm.FindContentChildren;

import java.util.Arrays;

public class FindContentChildrenImpl implements FindContentChildren {

    @Override
    public int findContentChildren(int[] g, int[] s) {
        int satisfy = 0, i = 0, j = 0;
        Arrays.sort(g);
        Arrays.sort(s);
        while (i < g.length && j < s.length) {
            if (g[i] <= s[j]) {
                i++;
                satisfy++;
            }
            j++;
        }
        return satisfy;
    }

}
