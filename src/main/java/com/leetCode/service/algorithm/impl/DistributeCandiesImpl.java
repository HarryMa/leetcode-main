package com.leetCode.service.algorithm.impl;

import com.leetCode.service.algorithm.DistributeCandies;

import java.util.*;
import java.util.stream.Collectors;

public class DistributeCandiesImpl implements DistributeCandies {

    @Override
    public int distributeCandies(int[] candyType) {
        Set<Integer> candyTypeSet = Arrays.stream(candyType).boxed().collect(Collectors.toSet());

        return Math.min(candyType.length / 2, candyTypeSet.size());
    }

}
