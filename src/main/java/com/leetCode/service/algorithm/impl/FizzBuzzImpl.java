package com.leetCode.service.algorithm.impl;

import com.leetCode.service.algorithm.FizzBuzz;

import java.util.ArrayList;
import java.util.List;

public class FizzBuzzImpl implements FizzBuzz {

    @Override
    public List<String> fizzBuzz(int n) {
        List<String> answer = new ArrayList<>();
        for (int i = 1; i <= n; i++) {
            StringBuilder word = new StringBuilder();
            if (i % 3 == 0) {
                word.append("Fizz");
            }
            if (i % 5 == 0) {
                word.append("Buzz");
            }

            if (word.length() == 0) {
                answer.add(String.valueOf(i));
            } else {
                answer.add(word.toString());
            }
        }
        
        return answer;
    }

}
