package com.leetCode.service.algorithm.impl;

import com.leetCode.service.algorithm.ConvertToTitle;

public class ConvertToTitleImpl implements ConvertToTitle {

    @Override
    public String convertToTitle(int columnNumber) {
        StringBuilder rowName = new StringBuilder();
        while (columnNumber > 0) {
            int num = columnNumber % 26;
            if (num == 0) {
                if (columnNumber <= 26) {
                    num = columnNumber;
                } else {
                    num = 26;
                }
                columnNumber = columnNumber / 26 - 1;
            } else {
                columnNumber = columnNumber / 26;
            }
            char column = (char) ('A' + num - 1);
            rowName.append(column);
        }

        return rowName.reverse().toString();
    }

    @Override
    public int titleToNumber(String columnTitle) {
        int number = 0;
        int length = columnTitle.length() - 1;
        for (int i = length; i >= 0 ; i--) {
            double value = Math.pow(26, length - i);
            int flag = columnTitle.charAt(i) - 64;
            number += value * flag;
        }

        return number;
    }

}
