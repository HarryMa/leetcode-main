package com.leetCode.service.algorithm.impl;

import com.leetCode.service.algorithm.HammingDistance;

public class HammingDistanceImpl implements HammingDistance {

    @Override
    public int hammingDistance(int x, int y) {
        int a = x ^ y;
        int count = 0;
        while (a != 0) {
            count++;
            a = a & (a - 1);
        }

        return count;
    }

}
