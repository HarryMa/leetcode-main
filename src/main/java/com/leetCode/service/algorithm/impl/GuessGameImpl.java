package com.leetCode.service.algorithm.impl;

import com.leetCode.service.algorithm.GuessGame;

public class GuessGameImpl implements GuessGame {

    @Override
    public int guessNumber(int n) {
        int min = 1, max = n, num = n/2;
        while (guess(num) != 0) {
            if (guess(num) == 1) {
                min = num;
                int median = num + (max - min) / 2;
                num = median == num ? num + 1 : median;
            } else {
                max = num;
                int median = num - (max - min) / 2;
                num = median == num ? num - 1 : median;
            }
        }
        return num;
    }

    /**
     * 与定好的数字来判断大小
     * -1：我选出的数字比你猜的数字小 pick < num
     * 1：我选出的数字比你猜的数字大 pick > num
     * 0：我选出的数字和你猜的数字一样。恭喜！你猜对了！pick == num
     *
     * @param num 给定的正整数
     * @return 大小
     */
    public int guess(int num) {
        int pick = 1702766719;

        return Integer.compare(pick, num);
    }
}
