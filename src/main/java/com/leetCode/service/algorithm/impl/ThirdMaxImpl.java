package com.leetCode.service.algorithm.impl;

import com.leetCode.service.algorithm.ThirdMax;

import java.util.Arrays;

public class ThirdMaxImpl implements ThirdMax {

    @Override
    public int thirdMax(int[] nums) {
        if (nums.length < 3) {
            Arrays.sort(nums);
            return nums[nums.length - 1];
        }

        int thirdMax = nums[nums.length - 1], flag = 0, i = nums.length - 1;
        while (flag < 2 && i >= 0) {
            if (nums[i] < thirdMax) {
                thirdMax = nums[i];
                flag++;
            }
            i--;
        }

        if (flag != 2) {
            return nums[nums.length - 1];
        }

        return thirdMax;
    }

}
