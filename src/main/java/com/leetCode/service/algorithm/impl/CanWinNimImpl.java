package com.leetCode.service.algorithm.impl;

import com.leetCode.service.algorithm.CanWinNim;

public class CanWinNimImpl implements CanWinNim {

    @Override
    public boolean canWinNim(int n) {
        return n % 4 != 0;
    }

}
