package com.leetCode.service.algorithm.impl;

import com.leetCode.service.algorithm.NextGreaterElement;

import java.util.Arrays;

public class NextGreaterElementImpl implements NextGreaterElement {

    @Override
    public int[] nextGreaterElement(int[] nums1, int[] nums2) {
        int[] ans = new int[nums1.length];
        for (int i = 0; i < nums1.length; i++) {
            int j = 0;
            while (nums1[i] != nums2[j]) {
                j++;
            }
            while (j < nums2.length && nums1[i] >= nums2[j]) {
                j++;
            }
            if (j < nums2.length) {
                ans[i] = nums2[j];
            } else {
                ans[i] = -1;
            }
        }

        return ans;
    }

}
