package com.leetCode.service.algorithm.impl;

import com.leetCode.service.algorithm.CanConstruct;

public class CanConstructImpl implements CanConstruct {

    @Override
    public boolean canConstruct(String ransomNote, String magazine) {
        if (ransomNote.length() > magazine.length()) {
            return false;
        }

        for (int i = 0; i < ransomNote.length(); i++) {
            String word = String.valueOf(ransomNote.charAt(i));
            int index = magazine.indexOf(word);
            if (index == -1) {
                return false;
            }
            magazine = magazine.replaceFirst(word, "");
        }

        return true;
    }

}
