package com.leetCode.service.algorithm.impl;

import com.leetCode.service.algorithm.FindPoisonedDuration;

public class FindPoisonedDurationImpl implements FindPoisonedDuration {

    @Override
    public int findPoisonedDuration(int[] timeSeries, int duration) {
        int durationTime = timeSeries[0] + duration - 1;
        int sum = 0;

        for (int i = 1; i < timeSeries.length; i++) {
            if (timeSeries[i] > durationTime) {
                sum += (durationTime - timeSeries[i - 1] + 1);
            } else {
                sum += (timeSeries[i] - timeSeries[i - 1]);
            }
            durationTime = timeSeries[i] + duration - 1;
        }

        sum += (durationTime - timeSeries[timeSeries.length - 1] + 1);

        return sum;
    }

}
