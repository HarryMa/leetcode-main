package com.leetCode.service.algorithm.impl;

import com.leetCode.service.algorithm.CheckRecord;

public class CheckRecordImpl implements CheckRecord {

    @Override
    public boolean checkRecord(String s) {
        boolean beLate = s.contains("LLL");
        int count = 0;
        while(s.indexOf("A") != -1) {
            s = s.substring(s.indexOf("A") + 1, s.length());
            count++;
        }
        boolean absent = count > 1;
        return !(beLate | absent);
    }

}
