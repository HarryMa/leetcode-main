package com.leetCode.service.algorithm.impl;

import com.leetCode.service.algorithm.FindComplement;

public class FindComplementImpl implements FindComplement {

    @Override
    public int findComplement(int num) {
        StringBuilder complement = new StringBuilder();
        while (num != 0) {
            if ((num & 1) == 1) {
                complement.append("0");
            } else {
                complement.append("1");
            }
            num >>= 1;
        }

        complement.reverse();
        return Integer.parseInt(complement.toString(), 2);
    }
}
