package com.leetCode.service.algorithm.impl;

import com.leetCode.service.algorithm.IslandPerimeter;

public class IslandPerimeterImpl implements IslandPerimeter {

    @Override
    public int islandPerimeter(int[][] grid) {
        int row = grid.length;
        int col = grid[0].length;
        int[] x = {-1, 1, 0, 0};
        int[] y = {0, 0, 1, -1};

        int sum = 0;
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                if (grid[i][j] == 1) {
                    int add = 0;
                    for (int k = 0; k < 4; k++) {
                        int iy = i + y[k];
                        int jx = j + x[k];
                        if (iy < 0 || iy >= row || jx < 0 || jx >= col || grid[iy][jx] == 0) {
                            add++;
                        }
                    }
                    sum += add;
                }
            }
        }

        return sum;
    }

}
