package com.leetCode.service.algorithm.impl;

import com.leetCode.service.algorithm.FindMaxConsecutiveOnes;

public class FindMaxConsecutiveOnesImpl implements FindMaxConsecutiveOnes {

    @Override
    public int findMaxConsecutiveOnes(int[] nums) {
        int max = 0, num = 0;
        for (int i : nums) {
            if (i == 1) {
                num++;
            } else {
                max = Math.max(max, num);
                num = 0;
            }
        }

        return Math.max(max, num);
    }

}
