package com.leetCode.service.algorithm.impl;

import com.leetCode.service.algorithm.ConstructRectangle;

import java.util.Arrays;

public class ConstructRectangleImpl implements ConstructRectangle {

    public static void main(String[] args) {
        System.out.println(Arrays.toString(test(6)));
    }

    public static int[] test(int area) {
        int length = 1, width = 1, flag = area;
        int[] num = {1, 1};
        while (length >= width && width <= area / 2) {
            if (area % width == 0) {
                length = area / width;
                if (Math.abs(length - width) < flag) {
                    flag = length - width;
                    num[0] = length;
                    num[1] = width;
                }
            }
            width++;
        }

        return num;
    }

    @Override
    public int[] constructRectangle(int area) {
        int length = 1, width = 1, flag = area;
        int[] num = new int[2];
        while (length >= width && width <= area / 2) {
            if (area % length == 0) {
                length = area / width;
                if (length - width < flag) {
                    flag = length - width;
                    num[0] = length;
                    num[1] = width;
                }
            }
            width++;
        }

        return num;
    }

}
