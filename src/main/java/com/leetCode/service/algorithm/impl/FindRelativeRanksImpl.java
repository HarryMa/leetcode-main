package com.leetCode.service.algorithm.impl;

import com.leetCode.service.algorithm.FindRelativeRanks;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class FindRelativeRanksImpl implements FindRelativeRanks {

    @Override
    public String[] findRelativeRanks(int[] score) {
        int length = score.length;
        int[] nums = Arrays.copyOf(score, length);
        Arrays.sort(nums);

        Map<Integer, String> map = new TreeMap<>();
        for (int i = length - 1; i >= 0; i--) {
            map.put(nums[i], String.valueOf(length - i));
        }

        String[] answer = new String[length];
        for (int i = 0; i < length; i++) {
            if ("1".equals(map.get(score[i]))) {
                answer[i] = "Gold Medal";
            } else if ("2".equals(map.get(score[i]))) {
                answer[i] = "Silver Medal";
            } else if ("3".equals(map.get(score[i]))) {
                answer[i] = "Bronze Medal";
            } else {
                answer[i] = map.get(score[i]);
            }
        }

        return answer;
    }
}
