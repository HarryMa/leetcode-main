package com.leetCode.service.algorithm.impl;

import com.leetCode.service.algorithm.ReadBinaryWatch;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ReadBinaryWatchImpl implements ReadBinaryWatch {

    @Override
    public List<String> readBinaryWatch(int turnedOn) {
        if (turnedOn > 8) {
            return Collections.emptyList();
        }

        List<String> timeList = new ArrayList<>();
        for (int h = 0; h < 12; h++) {
            for (int m = 0; m < 60; m++) {
                if (Integer.bitCount(h) + Integer.bitCount(m) == turnedOn) {
                    timeList.add(h + ":" + (m > 9 ? m : "0" + m) );
                }
            }
        }

        return timeList;
    }

}
