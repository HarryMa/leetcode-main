package com.leetCode.service.algorithm.impl;

import com.leetCode.service.algorithm.ArrangeCoins;

public class ArrangeCoinsImpl implements ArrangeCoins {

    @Override
    public int arrangeCoins(int n) {
        int i = 1;
        while (n >= i) {
            n = n - i;
            i++;
        }
        i--;

        return i;
    }

}
