package com.leetCode.service.algorithm;

public interface DistributeCandies {

    /**
     * 分糖果
     * 给你一个长度为n的整数数组candyType，返回：Alice在仅吃掉 n / 2 枚糖的情况下，可以吃到糖的最多种类数。
     *
     * @param candyType 糖果类型
     * @return 最多种类
     */
    int distributeCandies(int[] candyType);

}
