package com.leetCode.service.algorithm;

public interface NextGreaterElement {

    /**
     * 下一个更大元素 I
     * nums1中数字x的下一个更大元素是指x在nums2中对应位置右侧的第一个比x大的元素。
     * 给你两个没有重复元素的数组nums1和nums2，下标从0开始计数，其中nums1是nums2的子集。
     * 对于每个0 <= i < nums1.length，找出满足nums1[i] == nums2[j]的下标j，并且在nums2确定nums2[j]的下一个更大元素。
     * 如果不存在下一个更大元素，那么本次查询的答案是 -1。
     * 返回一个长度为nums1.length 的数组 ans 作为答案，满足ans[i]是如上所述的下一个更大元素 。
     *
     * nums1和nums2中所有整数 互不相同
     * nums1 中的所有整数同样出现在 nums2 中
     *
     * @param nums1 1 <= nums1.length <= nums2.length <= 1000, 0 <= nums1[i], nums2[i] <= 104
     * @param nums2 1 <= nums1.length <= nums2.length <= 1000, 0 <= nums1[i], nums2[i] <= 104
     * @return 下一个更大元素数组
     */
    int[] nextGreaterElement(int[] nums1, int[] nums2);
}
