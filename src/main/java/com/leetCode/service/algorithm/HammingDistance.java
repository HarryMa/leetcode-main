package com.leetCode.service.algorithm;

public interface HammingDistance {

    /**
     * 汉明距离
     * 两个整数之间的 汉明距离 指的是这两个数字对应二进制位不同的位置的数目。
     *
     * @param x 0 <= x <= 2^31 - 1
     * @param y 0 <= x <= 2^31 - 1
     * @return 二进制不同位置的数目
     */
    int hammingDistance(int x, int y);

}
