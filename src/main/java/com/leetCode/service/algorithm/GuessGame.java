package com.leetCode.service.algorithm;

public interface GuessGame {

    /**
     * 猜数字大小
     * 每轮游戏，我都会从 1 到 n 随机选择一个数字。 请你猜选出的是哪个数字。
     * 1 <= pick(我选出的数字) <= n
     *
     * @param n 1 <= n <= 2^31 - 1,
     * @return 我选出的数字
     */
    int guessNumber(int n);

}
