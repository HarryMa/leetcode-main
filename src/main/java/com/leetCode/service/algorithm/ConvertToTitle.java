package com.leetCode.service.algorithm;

public interface ConvertToTitle {

    /**
     * Excel表列名称
     * 给你一个整数 columnNumber ，返回它在 Excel 表中相对应的列名称。
     *
     * @param columnNumber 1 <= columnNumber <= 231 - 1
     * @return 在 Excel 表中相对应的列名称
     */
    String convertToTitle(int columnNumber);

    /**
     * Excel 表列序号
     * 给你一个字符串 columnTitle ，表示 Excel 表格中的列名称。返回该列名称对应的列序号。
     *
     * @param columnTitle 1 <= columnTitle.length <= 7, columnTitle 仅由大写英文组成, columnTitle 在范围 ["A", "FXSHRXW"] 内
     * @return 返回 Excel 表中相对应的列名称。
     */
    int titleToNumber(String columnTitle);

}
