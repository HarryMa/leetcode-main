package com.leetCode.service.algorithm;

public interface MinimumPathSum {

    /**
     * 给定一个包含非负整数的 m x n 网格 grid ，请找出一条从左上角到右下角的路径，使得路径上的数字总和为最小。
     *
     * @param grid 包含非负整数的网格
     * @return
     */
    int minPathSum(int[][] grid);

}
