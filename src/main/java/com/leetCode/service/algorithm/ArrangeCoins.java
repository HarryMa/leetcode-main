package com.leetCode.service.algorithm;

public interface ArrangeCoins {

    /**
     * 排列硬币
     * 你总共有 n 枚硬币，并计划将它们按阶梯状排列。对于一个由 k 行组成的阶梯，其第 i 行必须正好有 i 枚硬币。阶梯的最后一行 可能 是不完整的。
     *
     * @param n 1 <= n <= 2^31 - 1
     * @return 计算并返回可形成完整阶梯行的总行数
     */
    int arrangeCoins(int n);

}
