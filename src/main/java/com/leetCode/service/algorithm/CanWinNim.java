package com.leetCode.service.algorithm;

public interface CanWinNim {

    /**
     * Nim 游戏
     * 你和你的朋友，两个人一起玩 Nim 游戏：
     *
     * 桌子上有一堆石头。
     * 你们轮流进行自己的回合，你作为先手。
     * 每一回合，轮到的人拿掉1 - 3 块石头。
     * 拿掉最后一块石头的人就是获胜者。
     *
     * @param n 1 <= n <= 2^31 - 1
     * @return 是否能胜利
     */
    boolean canWinNim(int n);

}
