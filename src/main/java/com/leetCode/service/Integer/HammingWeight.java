package com.leetCode.service.Integer;

public interface HammingWeight {

    /**
     * 为1的个数
     * 编写一个函数，输入是一个无符号整数（以二进制串的形式），返回其二进制表达式中数字位数为 '1' 的个数（也被称为汉明重量）。
     *
     * @param n 输入必须是长度为 32 的 二进制串 。
     * @return 为1的个数
     */
    int hammingWeight(int n);

}
