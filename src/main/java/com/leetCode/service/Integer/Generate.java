package com.leetCode.service.Integer;

import java.util.List;

public interface Generate {

    /**
     * 杨辉三角
     * 给定一个非负整数 numRows，生成「杨辉三角」的前 numRows 行。
     *
     * @param numRows 1 <= numRows <= 30
     * @return 生成的「杨辉三角」
     */
    List<List<Integer>> generate(int numRows);

}
