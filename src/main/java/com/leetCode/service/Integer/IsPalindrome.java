package com.leetCode.service.Integer;

public interface IsPalindrome {

    /**
     * 回文数
     * 给你一个整数 x ，如果 x 是一个回文整数，返回 true ；否则，返回 false 。
     *
     * @param x -231 <= x <= 231 - 1
     * @return
     */
    boolean isPalindrome(int x);

}
