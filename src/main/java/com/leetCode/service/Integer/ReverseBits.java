package com.leetCode.service.Integer;

public interface ReverseBits {

    /**
     * 颠倒二进制位
     * 颠倒给定的 32 位无符号整数的二进制位。
     *
     * @param n 一个长度为 32 的二进制字符串
     * @return 颠倒后的二进制
     */
    int reverseBits(int n);

}
