package com.leetCode.service.Integer;

public interface Fib {

    /**
     * 509 斐波那契数
     * 斐波那契数，通常用F(n)表示，形成的序列称为 斐波那契数列。该数列由0和1开始，后面的每一项数字都是前面两项数字的和。也就是：
     * F(0) = 0，F(1)= 1
     * F(n) = F(n - 1) + F(n - 2)，其中 n > 1
     *
     * @param n 0 <= n <= 30
     * @return F(n)的结果
     */
    int fib(int n);

}
