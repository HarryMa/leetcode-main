package com.leetCode.service.Integer;

public interface ConvertToBase7 {

    /**
     * 七进制数
     * 给定一个整数 num，将其转化为 7 进制，并以字符串形式输出。
     *
     * @param num -10^7 <= num <= 10^7
     * @return 七进制数
     */
    String convertToBase7(int num);

}
