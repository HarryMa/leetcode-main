package com.leetCode.service.Integer;

public interface IsPerfectSquare {

    /**
     * 有效的完全平方数
     * 给定一个 正整数 num ，编写一个函数，如果 num 是一个完全平方数，则返回 true ，否则返回 false 。
     * 进阶：不要 使用任何内置的库函数，如sqrt。
     *
     * @param num 1 <= num <= 2^31 - 1
     * @return 是否是有效的完全平方数
     */
    boolean isPerfectSquare(int num);

}
