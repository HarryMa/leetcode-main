package com.leetCode.service.Integer;

import java.util.List;

public interface GetRow {

    /**
     * 杨辉三角 II
     * 给定一个非负索引 rowIndex，返回「杨辉三角」的第 rowIndex 行。
     *
     * @param rowIndex 0 <= rowIndex <= 33
     * @return 指定杨辉三角的行数据
     */
    List<Integer> getRow(int rowIndex);

}
