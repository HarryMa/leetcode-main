package com.leetCode.service.Integer;

public interface AddDigits {

    /**
     * 各位相加
     * 给定一个非负整数 num，反复将各个位上的数字相加，直到结果为一位数。
     *
     * @param num 非负整数
     * @return 最后结果的一位数
     */
    int addDigits(int num);

}
