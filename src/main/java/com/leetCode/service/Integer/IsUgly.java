package com.leetCode.service.Integer;

public interface IsUgly {

    /**
     * 丑数
     * 给你一个整数n，请你判断n是否为丑数
     *
     * @param n -2^31 <= n <= 2^31 - 1
     * @return 如果是，返回 true；否则，返回false 。
     */
    boolean isUgly(int n);

}
