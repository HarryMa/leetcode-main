package com.leetCode.service.Integer;

public interface CheckPerfectNumber {

    /**
     * 完美数
     * 对于一个正整数，如果它和除了它自身以外的所有 正因子 之和相等，我们称它为 「完美数」。
     * 给定一个整数n，如果是完美数，返回 true，否则返回 false
     *
     * @param num 1 <= num <= 10^8
     * @return 是否为完美数
     */
    boolean checkPerfectNumber(int num);

}
