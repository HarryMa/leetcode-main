package com.leetCode.service.Integer;

public interface IsHappy {

    /**
     * 快乐数
     * 对于一个正整数，每一次将该数替换为它每个位置上的数字的平方和。
     * 然后重复这个过程直到这个数变为 1，也可能是 无限循环 但始终变不到 1。
     * 如果 可以变为  1，那么这个数就是快乐数。
     *
     * @param n 1 <= n <= 231 - 1
     * @return 如果 n 是快乐数就返回 true ；不是，则返回 false 。
     */
    boolean isHappy(int n);

}
