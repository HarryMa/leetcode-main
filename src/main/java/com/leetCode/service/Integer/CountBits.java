package com.leetCode.service.Integer;

public interface CountBits {

    /**
     * 比特位计数
     * 给你一个整数 n ，对于 0 <= i <= n 中的每个 i ，计算其二进制表示中 1 的个数
     *
     * @param n 0 <= n <= 10^5
     * @return 返回一个长度为 n + 1 的数组 ans 作为答案
     */
    int[] countBits(int n);

}
