package com.leetCode.service.Integer.impl;

import com.leetCode.service.Integer.ToHex;

public class ToHexImpl implements ToHex {

    @Override
    public String toHex(int num) {
        if (num == 0) {
            return "0";
        }

        String[] hex = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f"};
        StringBuilder toHex = new StringBuilder();

        if (num > 0) {
            while (num > 0) {
                int index = num % 16;
                toHex.append(hex[index]);
                num = num / 16;
            }
        } else {
            num++;
            for (int i = 0; i < 8; i++) {
                if (num != 0) {
                    int index = num % 16;
                    toHex.append(hex[15 + index]);
                    num = num / 16;
                } else {
                    toHex.append("f");
                }
            }
        }

        return toHex.reverse().toString();
    }

}
