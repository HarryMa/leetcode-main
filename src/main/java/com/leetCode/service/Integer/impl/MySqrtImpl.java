package com.leetCode.service.Integer.impl;

import com.leetCode.service.Integer.MySqrt;

public class MySqrtImpl implements MySqrt {

    public int mySqrt(int x) {
        if (x == 0) {
            return 0;
        }

        for (int i = 1; i <= x/2; i++) {
            int a = x / i;
            if (i == a) {
                return i;
            } else if (a < i) {
                return a;
            }
        }

        return 1;
    }

}
