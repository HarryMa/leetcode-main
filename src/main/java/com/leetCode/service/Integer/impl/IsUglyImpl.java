package com.leetCode.service.Integer.impl;

import com.leetCode.service.Integer.IsUgly;

public class IsUglyImpl implements IsUgly {

    @Override
    public boolean isUgly(int n) {
        if (n <= 0) {
            return false;
        }

        int[] value = {2, 3, 5};
        for (int i : value) {
            while (n % i == 0) {
                n = n / i;
            }
        }

        return n == 1;
    }

}
