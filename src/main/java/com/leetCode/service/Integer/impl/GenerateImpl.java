package com.leetCode.service.Integer.impl;

import com.leetCode.service.Integer.Generate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class GenerateImpl implements Generate {

    @Override
    public List<List<Integer>> generate(int numRows) {
        List<List<Integer>> list = new ArrayList<>();
        List<Integer> lastList = Collections.singletonList(1);
        list.add(lastList);
        for (int i = 2; i <= numRows; i++) {
            List<Integer> nextList = new ArrayList<>();
            nextList.add(1);
            for (int j = 1; j < i - 1; j++) {
                nextList.add(lastList.get(j - 1) + lastList.get(j));
            }
            nextList.add(1);
            list.add(nextList);
            lastList = nextList;
        }
        return list;
    }

}
