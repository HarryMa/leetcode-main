package com.leetCode.service.Integer.impl;

import com.leetCode.service.Integer.IsPerfectSquare;

public class IsPerfectSquareImpl implements IsPerfectSquare {

    @Override
    public boolean isPerfectSquare(int num) {
        if (num == 1) {
            return true;
        }
        int left = 2;
        int right = num;
        while (left < right) {
            int quotient = num / left;
            int remainder = num % left;
            if (quotient == left && remainder == 0) {
                return true;
            }
            left++;
            right = quotient;
        }

        return false;
    }

}
