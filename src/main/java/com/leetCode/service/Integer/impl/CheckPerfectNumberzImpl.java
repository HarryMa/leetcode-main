package com.leetCode.service.Integer.impl;

import com.leetCode.service.Integer.CheckPerfectNumber;

import java.util.HashSet;
import java.util.Set;

public class CheckPerfectNumberzImpl implements CheckPerfectNumber {

    @Override
    public boolean checkPerfectNumber(int num) {
        Set<Integer> divisor = new HashSet<>();
        int value = (int) Math.sqrt(num);
        for (int i = 1; i <= value; i++) {
            if (num % i == 0) {
                divisor.add(i);
                divisor.add(num / i);
            }
        }
        divisor.remove(num);

        int sum = 0;
        for (Integer integer : divisor) {
            sum += integer;
        }
        return sum == num;
    }
}
