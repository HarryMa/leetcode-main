package com.leetCode.service.Integer.impl;

import com.leetCode.service.Integer.ConvertToBase7;

public class ConvertToBase7Impl implements ConvertToBase7 {

    @Override
    public String convertToBase7(int num) {
        if (num == 0) {
            return "0";
        }

        int i = Math.abs(num);
        StringBuilder baseSeven = new StringBuilder();
        while (i >= 0) {
            baseSeven.append(i % 7);
            i = i / 7;
        }

        if (num < 0) {
            baseSeven.append("-");
        }
        baseSeven.reverse();

        return baseSeven.toString();
    }

}
