package com.leetCode.service.Integer.impl;

import com.leetCode.service.Integer.HammingWeight;

public class HammingWeightImpl implements HammingWeight {

    @Override
    public int hammingWeight(int n) {
        int count = 0;
        for (int i = 0; i < 32 && n != 0; i++) {
            if ((n & 1) == 1) {
                count++;
            }
            n >>= 1;
        }

        return count;
    }

}
