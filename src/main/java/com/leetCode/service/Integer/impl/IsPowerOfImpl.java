package com.leetCode.service.Integer.impl;

import com.leetCode.service.Integer.IsPowerOf;

public class IsPowerOfImpl implements IsPowerOf {

    @Override
    public boolean isPowerOfTwo(int n) {
        if (n <= 0) {
            return false;
        }

        return (n & (n - 1)) == 0;
    }

    @Override
    public boolean isPowerOfThree(int n) {
        if (n <= 0) {
            return false;
        }

        while (n % 3 == 0) {
            n = n / 3;
        }

        return n == 1;
    }

    @Override
    public boolean isPowerOfFour(int n) {
        if (n <= 0) {
            return false;
        }

        return n % 3 == 1 && (n & (n - 1)) == 0;
    }

}
