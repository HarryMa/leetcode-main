package com.leetCode.service.Integer.impl;

import com.leetCode.service.Integer.AddDigits;

public class AddDigitsImpl implements AddDigits {

    @Override
    public int addDigits(int num) {
        String number = String.valueOf(num);
        String[] nums = number.split("");
        int sum = Integer.parseInt(nums[0]);
        for (int i = 1; i < nums.length; i++) {
            int j = Integer.parseInt(nums[i]);
            if (sum + j < 10) {
                sum += j;
            } else {
                sum = (sum + j) % 10 + 1;
            }
        }

        return sum;
    }

}
