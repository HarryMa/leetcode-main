package com.leetCode.service.Integer.impl;

import com.leetCode.service.Integer.Merge;

public class MergeImpl implements Merge {

    @Override
    public void merge(int[] nums1, int m, int[] nums2, int n) {
        for (int i = 0; i < n; i++) {
            int flag2 = nums2[i];
            for (int j = i; j < m + i; j++) {
                if (nums1[j] > flag2) {
                    int flag1 = nums1[j];
                    nums1[j] = flag2;
                    flag2 = flag1;
                }
            }
            nums1[m + i] = flag2;
        }
    }

}
