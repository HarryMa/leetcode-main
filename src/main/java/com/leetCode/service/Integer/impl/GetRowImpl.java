package com.leetCode.service.Integer.impl;

import com.leetCode.service.Integer.GetRow;

import java.util.ArrayList;
import java.util.List;

public class GetRowImpl implements GetRow {

    @Override
    public List<Integer> getRow(int rowIndex) {
        List<Integer> row = new ArrayList<>();
        row.add(1);
        for (int i = 1; i < rowIndex + 1; i++) {
            row.add(0);
            for (int j = i; j > 0; j--) {
                row.set(j, row.get(j) + row.get(j - 1));
            }
        }

        return row;
    }

}
