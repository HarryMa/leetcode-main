package com.leetCode.service.Integer.impl;

import com.leetCode.service.Integer.ReverseBits;

public class ReverseBitsImpl implements ReverseBits {

    @Override
    public int reverseBits(int n) {
        StringBuilder builder = new StringBuilder();
        String binaryString = Integer.toBinaryString(n);
        for (int i = binaryString.length(); i < 32; i++) {
            builder.append("0");
        }
        builder.append(binaryString);
        builder.reverse();

        return Integer.parseUnsignedInt(builder.toString(), 2);
    }

}
