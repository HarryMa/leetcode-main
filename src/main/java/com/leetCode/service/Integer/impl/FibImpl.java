package com.leetCode.service.Integer.impl;

import com.leetCode.service.Integer.Fib;

public class FibImpl implements Fib {

    @Override
    public int fib(int n) {
        if (n < 2) {
            return n;
        }

        return fib(n - 1) + fib(n - 2);
    }

}
