package com.leetCode.service.Integer.impl;

import com.leetCode.service.Integer.IsPalindrome;

public class IsPalindromeImpl implements IsPalindrome {

    public static void main(String[] args) {
        int x = 123321;
        System.out.println(new IsPalindromeImpl().isPalindrome(x));
    }

    @Override
    public boolean isPalindrome(int x) {
        if (x < 0) {
            return false;
        }

        char value[] = String.valueOf(x).toCharArray();
        int size = value.length / 2;
        for (int i = 0; i < size; i++) {
            int j = value.length - i - 1;
            if (value[i] != value[j]) {
                return false;
            }
        }

        return true;
    }

}
