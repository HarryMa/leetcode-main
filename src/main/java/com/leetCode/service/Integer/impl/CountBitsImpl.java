package com.leetCode.service.Integer.impl;

import com.leetCode.service.Integer.CountBits;

public class CountBitsImpl implements CountBits {

    @Override
    public int[] countBits(int n) {
        int[] bits = new int[n + 1];
        for (int i = 0; i <= n; i++) {
            int count = 0;
            int num = i;
            while (num != 0) {
                count ++;
                num = num & (num - 1);
            }
            bits[i] = count;
        }

        return bits;
    }

}
