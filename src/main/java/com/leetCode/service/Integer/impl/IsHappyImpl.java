package com.leetCode.service.Integer.impl;

import com.leetCode.service.Integer.IsHappy;

public class IsHappyImpl implements IsHappy {

    @Override
    public boolean isHappy(int n) {
        int sum = n;
        while (sum != 1) {
            n = sum;
            sum = 0;
            while (n > 0) {
                int a = n % 10;
                sum += Math.pow(a, 2);
                n = n / 10;
            }
            if (sum == 37) {
                return false;
            }
        }

        return true;
    }

}
