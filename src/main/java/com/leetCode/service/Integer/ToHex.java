package com.leetCode.service.Integer;

public interface ToHex {

    /**
     * 数字转换为十六进制数
     * 给定一个整数，编写一个算法将这个数转换为十六进制数。对于负整数，我们通常使用补码运算方法。
     *
     * @param num 整数
     * @return 转换后的十六进制数
     */
    String toHex(int num);

}
