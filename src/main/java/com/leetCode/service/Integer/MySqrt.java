package com.leetCode.service.Integer;

public interface MySqrt {

    /**
     * x 的平方根
     * 实现 int sqrt(int x) 函数。
     * 计算并返回 x 的平方根，其中 x 是非负整数。
     *
     * @param x 整数
     * @return 只保留整数的部分的平方根
     */
    int mySqrt(int x);

}
