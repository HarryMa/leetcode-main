package com.leetCode.service.Integer;

public interface IsPowerOf {

    /**
     * 2 的幂
     * 给你一个整数 n，请你判断该整数是否是 2 的幂次方。
     *
     * @param n -2^31 <= n <= 2^31 - 1
     * @return 如果是，返回 true ；否则，返回 false
     */
    boolean isPowerOfTwo(int n);

    /**
     * 3的幂
     * 给定一个整数，写一个函数来判断它是否是 3 的幂次方。
     *
     * @param n -2^31 <= n <= 2^31 - 1
     * @return 如果是，返回true；否则，返回false。
     */
    boolean isPowerOfThree(int n);

    /**
     * 4的幂
     * 给定一个整数，写一个函数来判断它是否是 4 的幂次方。
     *
     * @param n -2^31 <= n <= 2^31 - 1
     * @return 如果是，返回true；否则，返回false 。
     */
    boolean isPowerOfFour(int n);

}
