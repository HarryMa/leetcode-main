package com.leetCode.service.Integer;

public interface Merge {

    /**
     * 合并两个有序数组
     * 给你两个按 非递减顺序 排列的整数数组nums1 和 nums2，另有两个整数 m 和 n ，分别表示 nums1 和 nums2 中的元素数目。
     * 请你 合并 nums2 到 nums1 中，使合并后的数组同样按 非递减顺序 排列。
     *
     * @param nums1 nums1.length == m + n, -10^9 <= nums1[i], nums2[j] <= 10^9
     * @param m 0 <= m, n <= 200, 1 <= m + n <= 200
     * @param nums2 nums2.length == n, -10^9 <= nums1[i], nums2[j] <= 10^9
     * @param n 0 <= m, n <= 200, 1 <= m + n <= 200
     */
    void merge(int[] nums1, int m, int[] nums2, int n);

}
