package com.leetCode.service.String;

public interface AddStrings {

    /**
     * 字符串相加
     *
     * @param num1 长度都小于 5100,只包含数字 0-9,不包含任何前导零
     * @param num2 长度都小于 5100,只包含数字 0-9,不包含任何前导零
     * @return 相加结果
     */
    String addStrings(String num1, String num2);

}
