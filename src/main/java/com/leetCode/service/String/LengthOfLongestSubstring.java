package com.leetCode.service.String;

public interface LengthOfLongestSubstring {

    /**
     * 3、无重复字符的最长子串
     * 给定一个字符串s，请你找出其中不含有重复字符的最长子串的长度。
     *
     * @param s 0 <= s.length <= 5 * 10^4, s由英文字母、数字、符号和空格组成
     * @return 最长子串长度
     */
    int lengthOfLongestSubstring(String s);

}
