package com.leetCode.service.String;

public interface FindLUSlength {

    /**
     * 521 最长特殊序列 Ⅰ
     * 给你两个字符串a和b，请返回 这两个字符串中 最长的特殊序列 。如果不存在，则返回 -1。
     * 「最长特殊序列」定义如下：该序列为某字符串独有的最长子序列（即不能是其他字符串的子序列）。
     * 字符串s的子序列是在从s中删除任意数量的字符后可以获得的字符串。
     *
     * @param a 1 <= a.length <= 100, 由小写英文字母组成
     * @param b 1 <= b.length <= 100, 由小写英文字母组成
     * @return 最长特殊序列长度
     */
    int findLUSlength(String a, String b);

}
