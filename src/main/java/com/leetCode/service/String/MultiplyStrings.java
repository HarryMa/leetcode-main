package com.leetCode.service.String;

public interface MultiplyStrings {

    /**
     * 字符串相乘
     *
     * @param num1 长度小于110,只包含数字 0-9,均不以零开头，除非是数字 0 本身
     * @param num2 长度小于110,只包含数字 0-9,均不以零开头，除非是数字 0 本身
     * @return 相乘结果
     */
    String multiplyString (String num1, String num2);

}
