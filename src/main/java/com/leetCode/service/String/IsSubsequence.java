package com.leetCode.service.String;

public interface IsSubsequence {

    /**
     * 判断子序列
     * 给定字符串 s 和 t ，判断 s 是否为 t 的子序列。
     * 字符串的一个子序列是原始字符串删除一些（也可以不删除）字符而不改变剩余字符相对位置形成的新字符串。（例如，"ace"是"abcde"的一个子序列，而"aec"不是）。
     *
     * @param s 0 <= s.length <= 100
     * @param t 0 <= t.length <= 10^4, 两个字符串都只由小写字符组成。
     * @return 是否是子序列
     */
    boolean isSubsequence(String s, String t);

}
