package com.leetCode.service.String;

public interface FirstUniqChar {

    /**
     * 字符串中的第一个唯一字符
     * 给定一个字符串，找到它的第一个不重复的字符，并返回它的索引。如果不存在，则返回 -1
     *
     * @param s 只包含小写字母的字符串
     * @return 返回的索引
     */
    int firstUniqChar(String s);

}
