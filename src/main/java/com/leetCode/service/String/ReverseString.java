package com.leetCode.service.String;

public interface ReverseString {

    /**
     * 反转字符串
     * 编写一个函数，其作用是将输入的字符串反转过来。输入字符串以字符数组 s 的形式给出。
     *
     * @param s 1 <= s.length <= 10^5, s[i]都是ASCII码表中的可打印字符
     */
    void reverseString(char[] s);

    /**
     * 541 反转字符串 II
     * 给定一个字符串 s 和一个整数 k，从字符串开头算起，每计数至 2k 个字符，就反转这 2k 字符中的前 k 个字符。
     * 如果剩余字符少于 k 个，则将剩余字符全部反转。
     * 如果剩余字符小于 2k 但大于或等于 k 个，则反转前 k 个字符，其余字符保持原样。
     *
     * @param s 1 <= s.length <= 10^4, s 仅由小写英文组成
     * @param k 1 <= k <= 10^4
     * @return 返回转换后的字符串
     */
    String reverseStr(String s, int k);

    /**
     * 557 反转字符串中的单词 III
     *
     * @param s 1 <= s.length <= 5 * 10^4, 包含可打印的 ASCII 字符, 不包含任何开头或结尾空格,
     *          至少有一个词, 所有单词都用一个空格隔开。
     * @return 翻转后的字符串
     */
    String reverseWords(String s);

    /**
     * 反转字符串中的元音字母
     * 给你一个字符串 s ，仅反转字符串中的所有元音字母，并返回结果字符串。
     * 元音字母包括 'a'、'e'、'i'、'o'、'u'，且可能以大小写两种形式出现。
     *
     * @param s 1 <= s.length <= 3 * 10^5, s 由 可打印的 ASCII 字符组成
     * @return 翻转后分字符串
     */
    String reverseVowels(String s);

}
