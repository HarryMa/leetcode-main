package com.leetCode.service.String;

public interface WordPattern {

    /**
     * 单词规律
     * 给定一种规律pattern和一个字符串str ，判断 str是否遵循相同的规律。
     *
     *
     * @param pattern 只包含小写字母
     * @param s 包含了由单个空格分隔的小写字母
     * @return 是否符合规律
     */
    boolean wordPattern(String pattern, String s);

}
