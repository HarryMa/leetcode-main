package com.leetCode.service.String;

public interface IsPalindrome {

    /**
     * 验证回文串
     * 给定一个字符串，验证它是否是回文串，只考虑字母和数字字符，可以忽略字母的大小写。
     *
     * @param s 1 <= s.length <= 2 * 10^5, 字符串 s 由 ASCII 字符组成
     * @return 是否是回文串
     */
    boolean isPalindrome(String s);

}
