package com.leetCode.service.String;

public interface AddBinary {

    /**
     * 二进制求和
     * 给你两个二进制字符串，返回它们的和（用二进制表示）。
     *
     * @param a 每个字符串仅由字符 '0' 或 '1' 组成。1 <= a.length, b.length <= 10^4, 字符串如果不是 "0" ，就都不含前导零。
     * @param b 每个字符串仅由字符 '0' 或 '1' 组成。1 <= a.length, b.length <= 10^4, 字符串如果不是 "0" ，就都不含前导零。
     * @return 二进制和
     */
    String addBinary(String a, String b);

}
