package com.leetCode.service.String;

public interface RepeatedSubstringPattern {

    /**
     * 重复的子字符串
     * 给定一个非空的字符串，判断它是否可以由它的一个子串重复多次构成。
     *
     * @param s s.length() < 10000, 只有小写字母
     * @return 是否是重复的子字符串
     */
    boolean repeatedSubstringPattern(String s);

}
