package com.leetCode.service.String;

public interface LengthOfLastWord {

    /**
     * 最后一个单词的长度
     * 给你一个字符串 s，由若干单词组成，单词前后用一些空格字符隔开。返回字符串中最后一个单词的长度。
     *
     * @param s 1 <= s.length <= 10^4, s 仅有英文字母和空格 ' ' 组成, s 中至少存在一个单词
     * @return 最后单词的长度
     */
    int lengthOfLastWord(String s);

}
