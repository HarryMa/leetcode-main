package com.leetCode.service.String;

public interface LongestCommonPrefix {

    /**
     * 最长公共前缀
     * 编写一个函数来查找字符串数组中的最长公共前缀。
     * 如果不存在公共前缀，返回空字符串 ""。
     *
     * @param strs 1 <= strs.length <= 200, 0 <= strs[i].length <= 200, strs[i] 仅由小写英文字母组成
     * @return 最长公共前缀
     */
    String longestCommonPrefix(String[] strs);

}
