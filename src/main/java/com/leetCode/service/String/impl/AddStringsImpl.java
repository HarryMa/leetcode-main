package com.leetCode.service.String.impl;

import com.leetCode.service.String.AddStrings;

public class AddStringsImpl implements AddStrings {

    @Override
    public String addStrings(String num1, String num2) {
        int row1 = num1.length();
        int row2 = num2.length();

        StringBuilder sum = new StringBuilder();
        boolean flag = false;

        while (row1 > 0 || row2 > 0) {
            row1--;
            row2--;
            char c1 = row1 < 0 ? '0' : num1.charAt(row1);
            char c2 = row2 < 0 ? '0' : num2.charAt(row2);
            int flagSum = (c1 - '0') + (c2 - '0');
            if (flag) {
                flagSum += 1;
            }
            if (flagSum > 9) {
                sum.append(flagSum % 10);
                flag = true;
            } else {
                sum.append(flagSum);
                flag = false;
            }
        }
        if (flag) {
            sum.append("1");
        }
        return sum.reverse().toString();
    }

}
