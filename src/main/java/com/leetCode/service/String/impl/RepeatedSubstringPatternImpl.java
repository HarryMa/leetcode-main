package com.leetCode.service.String.impl;

import com.leetCode.service.String.RepeatedSubstringPattern;

import java.util.HashMap;
import java.util.Map;

public class RepeatedSubstringPatternImpl implements RepeatedSubstringPattern {

    @Override
    public boolean repeatedSubstringPattern(String s) {
        for (int i = 1; i * 2 <= s.length(); ++i) {
            if (s.length() % i == 0) {
                boolean match = true;
                for (int j = i; j < s.length(); ++j) {
                    if (s.charAt(j) != s.charAt(j - i)) {
                        match = false;
                        break;
                    }
                }
                if (match) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean easy(String s) {
        return (s + s).indexOf(s, 1) != s.length();
    }

}
