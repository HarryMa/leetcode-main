package com.leetCode.service.String.impl;

import com.leetCode.service.String.IsAnagram;

import java.util.Arrays;

public class IsAnagramImpl implements IsAnagram {

    @Override
    public boolean isAnagram(String s, String t) {
        if (s.length() != t.length()) {
            return false;
        }

        String[] sArr = s.split("");
        Arrays.sort(sArr);
        String[] tArr = t.split("");
        Arrays.sort(tArr);

        return Arrays.equals(sArr, tArr);
    }

}
