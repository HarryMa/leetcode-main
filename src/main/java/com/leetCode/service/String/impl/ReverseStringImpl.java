package com.leetCode.service.String.impl;

import com.leetCode.service.String.ReverseString;

public class ReverseStringImpl implements ReverseString {

    @Override
    public void reverseString(char[] s) {
        int length = s.length;
        char c;
        for (int i = 0; i < length / 2; i++) {
            c = s[i];
            s[i] = s[length - 1 - i];
            s[length - 1 - i] = c;
        }
    }

    /**
     * 将数据挨个按照规定处理,无用处理较多,效率低下
     * 下方添加官方解题思路,只调整了需要调换的数据,其他不动,效率比我这高多了
     *
     * @param s 1 <= s.length <= 10^4, s 仅由小写英文组成
     * @param k 1 <= k <= 10^4
     * @return 返回转换后的字符串
     */
    @Override
    public String reverseStr(String s, int k) {
        StringBuilder str = new StringBuilder();
        int i = 0;
        while (i + 2 * k <= s.length()) {
            for (int j = k - 1; j >= 0; j--) {
                str.append(s.charAt(i + j));
            }
            i += k;
            for (int j = 0; j < k; j++) {
                str.append(s.charAt(i + j));
            }
            i += k;
        }

        if (i + 2 * k != s.length()) {
            if (i + k >= s.length()) {
                for (int j = s.length() - 1; j >= i; j--) {
                    str.append(s.charAt(j));
                }
            } else {
                for (int j = k - 1; j >= 0; j--) {
                    str.append(s.charAt(i + j));
                }
                i += k;
                for (int j = i; j < s.length(); j++) {
                    str.append(s.charAt(j));
                }
            }
        }

        return str.toString();
    }

    public String official(String s, int k) {
        int n = s.length();
        char[] arr = s.toCharArray();
        for (int i = 0; i < n; i += 2 * k) {
            reverse(arr, i, Math.min(i + k, n) - 1);
        }
        return new String(arr);
    }

    public void reverse(char[] arr, int left, int right) {
        while (left < right) {
            char temp = arr[left];
            arr[left] = arr[right];
            arr[right] = temp;
            left++;
            right--;
        }
    }

    @Override
    public String reverseWords(String s) {
        String[] str = s.split(" ");
        StringBuilder reverseWords = new StringBuilder();
        for (String s1 : str) {
            for (int i = s1.length() - 1; i >= 0 ; i--) {
                reverseWords.append(s1.charAt(i));
            }
            reverseWords.append(" ");
        }

        reverseWords.deleteCharAt(reverseWords.length() - 1);
        return reverseWords.toString();
    }

    @Override
    public String reverseVowels(String s) {
        String vowel = "aAeEiIoOuU";
        char[] value = s.toCharArray();
        int i = 0, j = s.length() - 1;
        while (i < j) {
            if (!vowel.contains(String.valueOf(value[i]))) {
                i++;
            } else {
                if (vowel.contains(String.valueOf(value[j]))) {
                    char flag = value[i];
                    value[i] = value[j];
                    value[j] = flag;
                    i++;
                }
                j--;
            }
        }

        return String.valueOf(value);
    }

}
