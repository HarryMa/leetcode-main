package com.leetCode.service.String.impl;

import com.leetCode.service.String.IsIsomorphic;

import java.util.HashMap;
import java.util.Map;

public class IsIsomorphicImpl implements IsIsomorphic {

    @Override
    public boolean isIsomorphic(String s, String t) {
        Map<String, String> mapA = new HashMap<>();
        Map<String, String> mapB = new HashMap<>();
        for (int i = 0; i < s.length(); i++) {
            String a = String.valueOf(s.charAt(i));
            String b = String.valueOf(t.charAt(i));
            if (!mapA.containsKey(a) && !mapB.containsKey(b)) {
                mapA.put(a, b);
                mapB.put(b, a);
            } else {
                if (!a.equals(mapB.get(b)) || !b.equals(mapA.get(a))) {
                    return false;
                }
            }
        }

        return true;
    }

}
