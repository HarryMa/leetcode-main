package com.leetCode.service.String.impl;

import com.leetCode.service.String.CountSegments;
import org.apache.logging.log4j.util.Strings;

public class CountSegmentsImpl implements CountSegments {

    public static void main(String[] args) {
        System.out.println(test(""));
    }

    public static int test(String s) {
        String[] str = s.split(" ");
        int sum = 0;
        for (String s1 : str) {
            if (s1.length() != 0) {
                sum++;
            }
        }
        return sum;
    }

    @Override
    public int countSegments(String s) {
        int sum = 1;
        for (int i = 0; i < s.length(); i++) {
            if (!Character.isLowerCase(s.charAt(i)) && !Character.isUpperCase(s.charAt(i))) {
                sum++;
            }
        }

        return sum;
    }

}
