package com.leetCode.service.String.impl;

import com.leetCode.service.String.LengthOfLongestSubstring;

import java.util.HashMap;
import java.util.Map;

public class LengthOfLongestSubstringImpl implements LengthOfLongestSubstring {

    @Override
    public int lengthOfLongestSubstring(String s) {
        int index = 0, start = 0, end, maxLength = 0;
        int size = s.length();
        Map<Character, Integer> map = new HashMap<>();
        while (index < size)  {
            Character ch = s.charAt(index);
            if (map.containsKey(ch)) {
                end = index;
                if (map.get(ch) >= start && map.get(ch) <= end) {
                    maxLength = Math.max(maxLength, end - start);
                    start = map.get(ch) + 1;
                }
            }
            map.put(ch, index);
            index++;
        }

        return Math.max(maxLength, index - start);
    }

}
