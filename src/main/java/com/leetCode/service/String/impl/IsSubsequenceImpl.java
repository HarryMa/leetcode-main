package com.leetCode.service.String.impl;

import com.leetCode.service.String.IsSubsequence;

public class IsSubsequenceImpl implements IsSubsequence {

    @Override
    public boolean isSubsequence(String s, String t) {
        if (s ==null || t == null) {
            return false;
        }

        String[] str = s.split("");
        int index = -1;
        for (String a : str) {
            int falgIndex = t.indexOf(a, index + 1);
            if (falgIndex <= index) {
                return false;
            } else {
                index = falgIndex;
            }
        }
        return true;
    }

}
