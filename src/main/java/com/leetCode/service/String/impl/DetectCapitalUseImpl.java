package com.leetCode.service.String.impl;

import com.leetCode.service.String.DetectCapitalUse;

import java.util.Locale;

public class DetectCapitalUseImpl implements DetectCapitalUse {

    @Override
    public boolean detectCapitalUse(String word) {
        boolean flag = word.charAt(0) - 'a' >= 0;
        String other = word.substring(1);
        String lowerWord = other.toLowerCase();
        if (flag) {
            return other.equals(lowerWord);
        } else {
            String upperWord = other.toUpperCase();
            return other.equals(lowerWord) || other.equals(upperWord);
        }
    }

}
