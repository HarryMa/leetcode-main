package com.leetCode.service.String.impl;

import com.leetCode.service.String.IsPalindrome;

public class IsPalindromeImpl implements IsPalindrome {

    @Override
    public boolean isPalindrome(String s) {
        String lower = s.toLowerCase();
        lower = lower.replaceAll("[^0-9a-z]", "");
        int len = lower.length();
        for (int i = 0; i < len / 2; i++) {
            if (lower.charAt(i) != lower.charAt(len - 1 - i)) {
                return false;
            }
        }

        return true;
    }

}
