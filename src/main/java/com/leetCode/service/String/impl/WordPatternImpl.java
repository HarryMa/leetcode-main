package com.leetCode.service.String.impl;

import com.leetCode.service.String.WordPattern;

import java.util.HashMap;
import java.util.Map;

public class WordPatternImpl implements WordPattern {

    @Override
    public boolean wordPattern(String pattern, String s) {
        String[] str = s.split(" ");
        if (pattern.length() != str.length) {
            return false;
        }

        Map<String, String> keyValue = new HashMap<>();
        Map<String, String> valueKey = new HashMap<>();
        for (int i = 0; i < str.length; i++) {
            String key = String.valueOf(pattern.charAt(i));
            if (!keyValue.containsKey(key) && !valueKey.containsKey(str[i])) {
                keyValue.put(key, str[i]);
                valueKey.put(str[i], key);
                continue;
            }

            boolean trueKey = str[i].equals(keyValue.get(key));
            boolean trueValue = key.equals(valueKey.get(str[i]));
            if (!(trueKey && trueValue)) {
                return false;
            }
        }

        return true;
    }

}
