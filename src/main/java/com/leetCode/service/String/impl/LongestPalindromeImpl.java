package com.leetCode.service.String.impl;

import com.leetCode.service.String.LongestPalindrome;

import java.util.HashMap;
import java.util.Map;

public class LongestPalindromeImpl implements LongestPalindrome {

    @Override
    public int longestPalindrome(String s) {
        Map<Character, Integer> numMap = new HashMap<>();
        for (int i = 0; i < s.length(); i++) {
            if (numMap.containsKey(s.charAt(i))) {
                numMap.put(s.charAt(i), numMap.get(s.charAt(i)) + 1);
            } else {
                numMap.put(s.charAt(i), 1);
            }
        }

        int longest = 0;
        boolean along = false;
        for (Character character : numMap.keySet()) {
            longest += numMap.get(character);
            if (numMap.get(character) % 2 != 0) {
                longest--;
                along = true;
            }
        }

        if (along) {
            longest++;
        }

        return longest;
    }

}
