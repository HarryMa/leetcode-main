package com.leetCode.service.String.impl;

import com.leetCode.service.String.LicenseKeyFormatting;

public class LicenseKeyFormattingImpl implements LicenseKeyFormatting {

    @Override
    public String licenseKeyFormatting(String s, int k) {
        StringBuilder key = new StringBuilder();
        s = s.toUpperCase().replaceAll("-", "");
        int flag = 0;
        for (int i = s.length() - 1; i >= 0; i--) {
            if (flag < k) {
                key.append(s.charAt(i));
                flag++;
            } else {
                flag = 1;
                key.append("-");
                key.append(s.charAt(i));
            }
        }

        return key.reverse().toString();
    }

}
