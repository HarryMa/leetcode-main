package com.leetCode.service.String.impl;

import com.leetCode.service.String.FirstUniqChar;

public class FirstUniqCharImpl implements FirstUniqChar {

    @Override
    public int firstUniqChar(String s) {
        for (int i = 0; i < s.length(); i++) {
            String flag = String.valueOf(s.charAt(i));
            if (s.indexOf(flag) == i && s.lastIndexOf(flag) == i) {
                return i;
            }
        }

        return -1;
    }

}
