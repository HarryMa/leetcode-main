package com.leetCode.service.String.impl;

import com.leetCode.service.String.FindLUSlength;

public class FindLUSlengthImpl implements FindLUSlength {

    @Override
    public int findLUSlength(String a, String b) {
        if (!a.equals(b)) {
            return Math.max(a.length(), b.length());
        }

        return 0;
    }

}
