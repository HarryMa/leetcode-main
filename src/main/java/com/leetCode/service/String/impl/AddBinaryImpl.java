package com.leetCode.service.String.impl;

import com.leetCode.service.String.AddBinary;

public class AddBinaryImpl implements AddBinary {

    @Override
    public String addBinary(String a, String b) {
        int lenA = a.length() - 1;
        int lenB = b.length() - 1;
        StringBuilder sum = new StringBuilder();
        int carryBit = 0;

        while (lenA >= 0 || lenB >= 0) {
            char charA = lenA < 0 ? '0' : a.charAt(lenA);
            char charB = lenB < 0 ? '0' : b.charAt(lenB);

            if (charA == '1' && charB == '1') {
                sum.append(carryBit);
                carryBit = 1;
            } else if (charA == '0' && charB == '0') {
                sum.append(carryBit);
                carryBit = 0;
            } else {
                if (carryBit == 1) {
                    sum.append(0);
                } else {
                    sum.append(1);
                }
            }

            lenA--;
            lenB--;
        }

        if (carryBit == 1) {
            sum.append(carryBit);
        }

        return sum.reverse().toString();
    }

}
