package com.leetCode.service.String.impl;

import com.leetCode.service.String.AddStrings;
import com.leetCode.service.String.MultiplyStrings;
import org.springframework.beans.factory.annotation.Autowired;

public class MultiplyStringsImpl implements MultiplyStrings {

    @Autowired
    private AddStrings addStrings;

    @Override
    public String multiplyString(String num1, String num2) {
        if (num1.equals("0") || num2.equals("0")) {
            return "0";
        }
        int row = num1.length();
        String multiply = "0";
        StringBuilder flag = new StringBuilder();
        for (int i = row - 1; i >= 0; i--) {
            String sum = multiplyOne(num1.charAt(i), num2) + flag;
            multiply = addStrings.addStrings(sum, multiply);
            flag.append("0");
        }
        return multiply;
    }

    /**
     * 将一个数与一个字符串相乘
     *
     * @param c 一个字符串
     * @param num 长度小于110,只包含数字 0-9,均不以零开头，除非是数字 0 本身
     * @return 相乘结果
     */
    private String multiplyOne(char c, String num) {
        int value1 = c - '0';
        int row = num.length();
        int more = 0;
        StringBuilder multiplyOne = new StringBuilder();
        for (int i = row - 1; i >= 0; i--) {
            int value2 = num.charAt(i) - '0';
            int product = value1 * value2 + more;
            if (product > 9) {
                more = product / 10;
                multiplyOne.append(product % 10);
            } else {
                more = 0;
                multiplyOne.append(product);
            }
        }
        if (more != 0) {
            multiplyOne.append(more);
        }

        return multiplyOne.reverse().toString();
    }

}
