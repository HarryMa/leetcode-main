package com.leetCode.service.String.impl;

import com.leetCode.service.String.FindWords;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FindWordsImpl implements FindWords {

    @Override
    public String[] findWords(String[] words) {
        String one = "qwertyuiopQWERTYUIOP";
        String two = "asdfghjklASDFGHJKL";
        String three = "zxcvbnmZXCVBNM";
        List<String> list = new ArrayList<>();
        boolean flag;

        for (String word : words) {
            flag = true;
            String row = "";
            if (one.indexOf(word.charAt(0)) != -1) {
                row = one;
            } else if (two.indexOf(word.charAt(0)) != -1) {
                row = two;
            } else if (three.indexOf(word.charAt(0)) != -1) {
                row = three;
            }
            for (int i = 1; i < word.length(); i++) {
                if (row.indexOf(word.charAt(i)) == -1) {
                    flag = false;
                    break;
                }
            }
            if (flag) {
                list.add(word);
            }
        }

        String[] ans = new String[list.size()];
        for (int i = 0; i < list.size(); i++) {
            ans[i] = list.get(i);
        }

        return ans;
    }

}
