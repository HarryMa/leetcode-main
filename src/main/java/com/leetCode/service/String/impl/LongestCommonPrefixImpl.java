package com.leetCode.service.String.impl;

import com.leetCode.service.String.LongestCommonPrefix;

public class LongestCommonPrefixImpl implements LongestCommonPrefix {

    @Override
    public String longestCommonPrefix(String[] strs) {
        char[] chars = strs[0].toCharArray();
        for (int i = 1; i < strs.length; i++) {
            char[] crrChar = strs[i].toCharArray();
            char[] sameChar = new char[strs[0].length()];
            for (int j = 0; j < (Math.min(chars.length, crrChar.length)); j++) {
                if (chars[j] == crrChar[j]) {
                    sameChar[j] = chars[j];
                } else {
                    break;
                }
            }
            chars = sameChar;
            if (chars.length == 0) {
                return "";
            }
        }

        return String.valueOf(chars).trim();
    }

}
