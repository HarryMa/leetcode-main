package com.leetCode.service.String.impl;

import com.leetCode.service.String.StrStr;

public class StrStrImpl implements StrStr {

    @Override
    public int strStr(String haystack, String needle) {
        int len1 = haystack.length();
        int len2 = needle.length();
        if (len2 == 0) {
            return 0;
        } else if (len2 > len1) {
            return -1;
        }

        int i = 0, j = 0, index = 0;
        while (i < len1) {
            if (haystack.charAt(i) == needle.charAt(j)) {
                if (j == 0) {
                    index = i;
                }
                j++;
            } else if (j != 0) {
                j = 0;
                i = index;
            }
            i++;
            if (j == len2) {
                return i - j;
            }
        }

        return -1;
    }

}
