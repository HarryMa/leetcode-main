package com.leetCode.service.String.impl;

import com.leetCode.service.String.LengthOfLastWord;

public class LengthOfLastWordImpl implements LengthOfLastWord {

    @Override
    public int lengthOfLastWord(String s) {
        int len = s.length() - 1;
        int count = 0;
        while (len >= 0) {
            if (s.charAt(len) == ' ' && count != 0) {
                break;
            } else if (s.charAt(len) != ' ') {
                count++;
            }
            len--;
        }

        return count;
    }

}
