package com.leetCode.service.String.impl;

import com.leetCode.service.String.CheckRecord;

public class CheckRecordImpl implements CheckRecord {

    @Override
    public boolean checkRecord(String s) {
        boolean beLate = s.contains("LLL");
        int count = 0;
        while(s.contains("A")) {
            s = s.substring(s.indexOf("A") + 1);
            count++;
        }
        boolean absent = count > 1;
        return !(beLate | absent);
    }

}
