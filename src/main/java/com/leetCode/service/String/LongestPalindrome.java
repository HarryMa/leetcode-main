package com.leetCode.service.String;

public interface LongestPalindrome {

    /**
     * 最长回文串
     * 给定一个包含大写字母和小写字母的字符串，找到通过这些字母构造成的最长的回文串。
     * 在构造过程中，请注意区分大小写。比如 "Aa" 不能当做一个回文字符串。
     *
     * @param s s.length() < 1010, 都由字母组成
     * @return 最长回文串长度
     */
    int longestPalindrome(String s);

}
