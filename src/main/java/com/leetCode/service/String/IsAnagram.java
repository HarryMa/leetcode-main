package com.leetCode.service.String;

public interface IsAnagram {

    /**
     * 有效的字母异位词
     * 给定两个字符串 s 和 t ，编写一个函数来判断 t 是否是 s 的字母异位词。
     *
     * @param s 1 <= s.length, t.length <= 5 * 10^4, s和t仅包含小写字母
     * @param t 1 <= s.length, t.length <= 5 * 10^4, s和t仅包含小写字母
     * @return 是否是字母异位词
     */
    boolean isAnagram(String s, String t);

}
