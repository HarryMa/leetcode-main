package com.leetCode.service.String;

public interface CountSegments {

    /**
     * 字符串中的单词数
     * 统计字符串中的单词个数，这里的单词指的是连续的不是空格的字符。
     *
     * @param s 不包括任何不可打印的字符
     * @return 单词数量
     */
    int countSegments(String s);

}
