package com.leetCode.service.String;

public interface LicenseKeyFormatting {

    /**
     * 密钥格式化
     * 有一个密钥字符串 S ，只包含字母，数字以及 '-'（破折号）。其中， N 个 '-' 将字符串分成了 N+1 组。
     * 给你一个数字 K，请你重新格式化字符串，使每个分组恰好包含 K 个字符。
     * 特别地，第一个分组包含的字符个数必须小于等于 K，但至少要包含 1 个字符。两个分组之间需要用 '-'（破折号）隔开，
     * 并且将所有的小写字母转换为大写字母。
     *
     * @param s 非空, 只包含字母数字（a-z，A-Z，0-9）以及破折号'-'
     * @param k 整数
     * @return 格式化后的字符串
     */
    String licenseKeyFormatting(String s, int k);

}
