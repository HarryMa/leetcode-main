package com.leetCode.service.String;

public interface StrStr {

    /**
     * 实现 strStr()
     * 给你两个字符串haystack和needle，请你在haystack字符串中找出needle字符串出现的第一个位置（下标从 0 开始）。如果不存在，则返回-1
     *
     * @param haystack 0 <= haystack.length <= 5 * 104
     * @param needle 0 <= needle.length <= 5 * 104
     * @return 位置
     */
    int strStr(String haystack, String needle);

}
