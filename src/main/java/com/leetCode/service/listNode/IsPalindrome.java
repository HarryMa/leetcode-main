package com.leetCode.service.listNode;

import com.leetCode.provide.ListNode;

public interface IsPalindrome {

    /**
     * 回文链表
     * 给你一个单链表的头节点 head ，请你判断该链表是否为回文链表。
     *
     * @param head 链表中节点数目在范围[1, 10^5]内, 0 <= Node.val <= 9
     * @return 是否是回文链表
     */
    boolean isPalindrome(ListNode head);

}
