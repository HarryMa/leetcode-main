package com.leetCode.service.listNode;

import com.leetCode.provide.ListNode;

public interface DeleteNode {

    /**
     * 删除链表中的节点
     * 请编写一个函数，使其可以删除某个链表中给定的（非末尾）节点。传入函数的唯一参数为 要被删除的节点 。
     *
     * @param node 链表至少包含两个节点。链表中所有节点的值都是唯一的。给定的节点为非末尾节点并且一定是链表中的一个有效节点。
     */
    void deleteNode(ListNode node);

}
