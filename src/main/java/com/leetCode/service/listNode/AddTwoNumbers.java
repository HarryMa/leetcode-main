package com.leetCode.service.listNode;

import com.leetCode.provide.ListNode;

public interface AddTwoNumbers {

    /**
     * 2、两数相加
     * 给你两个非空的链表，表示两个非负的整数。它们每位数字都是按照逆序的方式存储的，并且每个节点只能存储一位数字。
     * 请你将两个数相加，并以相同形式返回一个表示和的链表。
     * 你可以假设除了数字0之外，这两个数都不会以0开头。
     *
     * @param l1 每个链表中的节点数在范围[1, 100]内, 0 <= Node.val <= 9
     * @param l2 每个链表中的节点数在范围[1, 100]内, 0 <= Node.val <= 9
     * @return 相加的和
     */
    ListNode addTwoNumbers(ListNode l1, ListNode l2);

}
