package com.leetCode.service.listNode;

import com.leetCode.provide.ListNode;

public interface MergeTwoLists {

    /**
     * 合并两个有序链表
     * 将两个升序链表合并为一个新的 升序 链表并返回。新链表是通过拼接给定的两个链表的所有节点组成的。
     * 两个链表的节点数目范围是 [0, 50]
     *
     * @param l1 -100 <= Node.val <= 100, l1为非递减顺序排列
     * @param l2 -100 <= Node.val <= 100, l2为非递减顺序排列
     * @return
     */
    ListNode mergeTwoLists(ListNode l1, ListNode l2);

}