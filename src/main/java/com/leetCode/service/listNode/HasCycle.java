package com.leetCode.service.listNode;

import com.leetCode.provide.ListNode;

public interface HasCycle {

    /**
     * 环形链表
     * 给定一个链表，判断链表中是否有环。
     * 如果链表中有某个节点，可以通过连续跟踪 next 指针再次到达，则链表中存在环。
     *
     * @param head 链表中节点的数目范围是 [0, 10^4], -10^5 <= Node.val <= 10^5
     * @return 如果链表中存在环，则返回 true 。 否则，返回 false 。
     */
    boolean hasCycle(ListNode head);

}
