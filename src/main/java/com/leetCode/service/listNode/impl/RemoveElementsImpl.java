package com.leetCode.service.listNode.impl;

import com.leetCode.provide.ListNode;
import com.leetCode.service.listNode.RemoveElements;

public class RemoveElementsImpl implements RemoveElements {

    @Override
    public ListNode removeElements(ListNode head, int val) {
        ListNode listNode = head;
        while (listNode != null && listNode.getNext() != null) {
            if (listNode.getNext().getVal() == val) {
                listNode.setNext(listNode.getNext().getNext());
            } else {
                listNode.setNext(listNode.getNext());
            }
        }

        if (head != null && head.getVal() == val) {
            head.setNext(head.getNext());
        }

        return head;
    }

}
