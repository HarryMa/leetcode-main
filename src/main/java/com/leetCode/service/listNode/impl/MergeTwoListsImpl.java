package com.leetCode.service.listNode.impl;

import com.leetCode.provide.ListNode;
import com.leetCode.service.listNode.MergeTwoLists;

public class MergeTwoListsImpl implements MergeTwoLists {

    @Override
    public ListNode mergeTwoLists(ListNode l1, ListNode l2) {
        if (l1 == null && l2 == null) {
            return null;
        }
        ListNode returnListNode = new ListNode();
        ListNode i = l1;
        ListNode j = l2;
        ListNode listNode = returnListNode;
        while (i != null || j != null) {
            if (i == null) {
                listNode.setVal(j.getVal());
                j = j.getNext();
            } else if (j == null) {
                listNode.setVal(i.getVal());
                i = i.getNext();
            } else if (i.getVal() < j.getVal()) {
                listNode.setVal(i.getVal());
                i = i.getNext();
            } else {
                listNode.setVal(j.getVal());
                j = j.getNext();
            }

            if (i != null || j != null) {
                listNode.setNext(new ListNode());
                listNode = listNode.getNext();
            }
        }
        listNode = null;

        return returnListNode;
    }

}