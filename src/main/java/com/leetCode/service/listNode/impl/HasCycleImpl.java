package com.leetCode.service.listNode.impl;

import com.leetCode.provide.ListNode;
import com.leetCode.service.listNode.HasCycle;

public class HasCycleImpl implements HasCycle {

    @Override
    public boolean hasCycle(ListNode head) {
        if (head == null || head.getNext() == null) {
            return false;
        }

        ListNode whileList1 = head;
        ListNode whileList2 = head.getNext();

        while (whileList1 != whileList2) {
            if (whileList2 == null || whileList2.getNext() == null) {
                return false;
            }
            whileList1 = whileList1.getNext();
            whileList2 = whileList2.getNext().getNext();
        }

        return true;
    }

}
