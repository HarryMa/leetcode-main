package com.leetCode.service.listNode.impl;

import com.leetCode.provide.ListNode;
import com.leetCode.service.listNode.GetIntersectionNode;

public class GetIntersectionNodeImpl implements GetIntersectionNode {

    @Override
    public ListNode getIntersectionNode(ListNode headA, ListNode headB) {
        ListNode whileList1 = headA;
        ListNode whileList2 = headB;

        while (whileList1 != null) {
            if (whileList1.equals(whileList2)) {
                return whileList1;
            }

            if (whileList2.getNext() != null) {
                whileList2 = whileList2.getNext();
            } else {
                whileList1 = whileList1.getNext();
                whileList2 = headB;
            }
        }

        return null;
    }

}
