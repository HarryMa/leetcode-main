package com.leetCode.service.listNode.impl;

import com.leetCode.provide.ListNode;
import com.leetCode.service.listNode.DeleteDuplicates;

public class DeleteDuplicatesImpl implements DeleteDuplicates {

    @Override
    public ListNode deleteDuplicates(ListNode head) {
        ListNode temporary = head;
        while (temporary != null && temporary.getNext() != null) {
            if (temporary.getVal() == temporary.getNext().getVal()) {
                temporary.setNext(temporary.getNext().getNext());
            } else {
                temporary = temporary.getNext();
            }
        }

        return head;
    }

}
