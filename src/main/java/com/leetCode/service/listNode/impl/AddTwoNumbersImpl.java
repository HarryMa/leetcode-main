package com.leetCode.service.listNode.impl;

import com.leetCode.provide.ListNode;
import com.leetCode.service.listNode.AddTwoNumbers;

public class AddTwoNumbersImpl implements AddTwoNumbers {

    @Override
    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        ListNode sumListNode = new ListNode();
        sumListNode.setVal((l1.getVal() + l2.getVal()) % 10);
        ListNode operation = sumListNode;
        int carry = (l1.getVal() + l2.getVal()) / 10;

        while (l1.getNext() != null || l2.getNext() != null || carry == 1) {
            l1 = l1.getNext() == null ? new ListNode() : l1.getNext();
            l2 = l2.getNext() == null ? new ListNode() : l2.getNext();
            int sum = l1.getVal() + l2.getVal() + carry;
            carry = sum / 10;
            operation.setNext(new ListNode());
            operation = operation.getNext();
            operation.setVal(sum % 10);
        }
        return sumListNode;
    }

}
