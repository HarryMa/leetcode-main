package com.leetCode.service.listNode.impl;

import com.leetCode.provide.ListNode;
import com.leetCode.service.listNode.ReverseList;

public class ReverseListImpl implements ReverseList {

    @Override
    public ListNode reverseList(ListNode head) {
        ListNode listNode = null;
        while (head != null) {
            listNode = new ListNode(head.getVal(), listNode);
            head = head.getNext();
        }

        return listNode;
    }

}
