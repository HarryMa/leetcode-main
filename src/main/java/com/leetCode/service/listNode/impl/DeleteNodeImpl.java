package com.leetCode.service.listNode.impl;

import com.leetCode.provide.ListNode;
import com.leetCode.service.listNode.DeleteNode;

public class DeleteNodeImpl implements DeleteNode {

    @Override
    public void deleteNode(ListNode node) {
        ListNode nextNode = node.getNext();
        node.setVal(nextNode.getVal());
        node.setNext(nextNode.getNext());
    }

}
