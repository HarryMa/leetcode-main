package com.leetCode.service.listNode.impl;

import com.leetCode.provide.ListNode;
import com.leetCode.service.listNode.IsPalindrome;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class IsPalindromeImpl implements IsPalindrome {

    @Override
    public boolean isPalindrome(ListNode head) {
        if (head.getNext() == null) {
            return true;
        }

        List<Integer> numberList = new ArrayList<>();
        while (head != null) {
            numberList.add(head.getVal());
            head = head.getNext();
        }

        int size = numberList.size();
        for (int i = 0; i < size / 2; i++) {
            if (!Objects.equals(numberList.get(i), numberList.get(size - 1 - i))) {
                return false;
            }
        }

        return true;
    }

}
