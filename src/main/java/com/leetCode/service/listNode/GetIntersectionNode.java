package com.leetCode.service.listNode;

import com.leetCode.provide.ListNode;

public interface GetIntersectionNode {

    /**
     * 相交链表
     * 给你两个单链表的头节点 headA 和 headB ，请你找出并返回两个单链表相交的起始节点。如果两个链表没有交点，返回 null 。
     * listA 中节点数目为 m
     * listB 中节点数目为 n
     * 如果 listA 和 listB 没有交点，intersectVal 为 0
     * 如果 listA 和 listB 有交点，intersectVal == listA[skipA + 1] == listB[skipB + 1]
     *
     * @param headA 0 <= m, n <= 3 * 10^4, 1 <= Node.val <= 10^5, 0 <= skipA <= m
     * @param headB 0 <= skipB <= n
     * @return 返回相交的点
     */
    ListNode getIntersectionNode(ListNode headA, ListNode headB);

}
