package com.leetCode.service.listNode;

import com.leetCode.provide.ListNode;

public interface RemoveElements {

    /**
     * 移除链表元素
     * 给你一个链表的头节点 head 和一个整数 val ，请你删除链表中所有满足 Node.val == val 的节点，并返回 新的头节点 。
     *
     * @param head 列表中的节点数目在范围 [0, 10^4] 内
     * @param val 1 <= Node.val <= 50, 0 <= val <= 50
     * @return 新的头节点
     */
    ListNode removeElements(ListNode head, int val);

}
