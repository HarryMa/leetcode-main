package com.leetCode.service.listNode;

import com.leetCode.provide.ListNode;

public interface DeleteDuplicates {

    /**
     * 删除排序链表中的重复元素
     * 存在一个按升序排列的链表，给你这个链表的头节点 head ，请你删除所有重复的元素，使每个元素 只出现一次 。
     *
     * @param head 链表中节点数目在范围 [0, 300] 内, -100 <= Node.val <= 100
     * @return 返回同样按升序排列的结果链表。
     */
    ListNode deleteDuplicates(ListNode head);

}
