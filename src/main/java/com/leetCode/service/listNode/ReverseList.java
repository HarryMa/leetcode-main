package com.leetCode.service.listNode;

import com.leetCode.provide.ListNode;

public interface ReverseList {

    /**
     * 反转链表
     * 给你单链表的头节点 head ，请你反转链表，并返回反转后的链表。
     *
     * @param head 链表中节点的数目范围是 [0, 5000], -5000 <= Node.val <= 5000
     * @return 反转后的链表
     */
    ListNode reverseList(ListNode head);

}
