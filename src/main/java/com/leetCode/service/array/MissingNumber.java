package com.leetCode.service.array;

public interface MissingNumber {

    /**
     * 丢失的数字
     * 给定一个包含 [0, n] 中 n 个数的数组 nums ，找出 [0, n] 这个范围内没有出现在数组中的那个数。
     *
     * @param nums n == nums.length, 1 <= n <= 10^4, 0 <= nums[i] <= n, nums 中的所有数字都 独一无二
     * @return 没有出现的那个数
     */
    int missingNumber(int[] nums);

}
