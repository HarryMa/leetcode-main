package com.leetCode.service.array;

import java.util.List;

public interface FindDisappearedNumbers {

    /**
     * 找到所有数组中消失的数字
     * 给你一个含 n 个整数的数组 nums ，其中 nums[i] 在区间 [1, n] 内。
     * 请你找出所有在 [1, n] 范围内但没有出现在 nums 中的数字，并以数组的形式返回结果。
     * n == nums.length, 1 <= n <= 10^5
     *
     * @param nums 1 <= n <= 10^5
     * @return 不存在的数字
     */
    List<Integer> findDisappearedNumbers(int[] nums);
}
