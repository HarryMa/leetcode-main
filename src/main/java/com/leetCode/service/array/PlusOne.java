package com.leetCode.service.array;

public interface PlusOne {

    /**
     * 加一
     * 给定一个由 整数 组成的 非空 数组所表示的非负整数，在该数的基础上加一。
     * 最高位数字存放在数组的首位， 数组中每个元素只存储单个数字。
     *
     * @param digits 1 <= digits.length <= 100, 0 <= digits[i] <= 9
     * @return 加一后的数组
     */
    int[] plusOne(int[] digits);

}
