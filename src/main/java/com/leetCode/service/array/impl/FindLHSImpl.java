package com.leetCode.service.array.impl;

import com.leetCode.service.array.FindLHS;

import java.util.HashMap;
import java.util.Map;

public class FindLHSImpl implements FindLHS {

    @Override
    public int findLHS(int[] nums) {
        Map<Integer, Integer> numSizeMap = new HashMap<>();
        for (int num : nums) {
            if (numSizeMap.containsKey(num)) {
                numSizeMap.put(num, numSizeMap.get(num) + 1);
            } else {
                numSizeMap.put(num, 1);
            }
        }

        int sum = 0;
        for (Integer integer : numSizeMap.keySet()) {
            if (numSizeMap.containsKey(integer - 1)) {
                sum = Math.max(sum, numSizeMap.get(integer) + numSizeMap.get(integer - 1));
            }
        }
        return sum;
    }

}
