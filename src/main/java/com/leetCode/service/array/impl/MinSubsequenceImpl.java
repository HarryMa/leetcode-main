package com.leetCode.service.array.impl;

import com.leetCode.service.array.MinSubsequence;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MinSubsequenceImpl implements MinSubsequence {

    @Override
    public List<Integer> minSubsequence(int[] nums) {
        List<Integer> checkedList = new ArrayList<>();
        Arrays.sort(nums);
        int residualNum = Arrays.stream(nums).sum();
        int checkedNum = 0;
        int index = nums.length - 1;
        while (index >= 0) {
            checkedList.add(nums[index]);
            checkedNum += nums[index];
            residualNum -= nums[index];
            index = checkedNum > residualNum ? -1 : index - 1;
        }

        return checkedList;
    }

}
