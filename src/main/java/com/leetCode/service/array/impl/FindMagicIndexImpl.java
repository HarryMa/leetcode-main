package com.leetCode.service.array.impl;

import com.leetCode.service.array.FindMagicIndex;

public class FindMagicIndexImpl implements FindMagicIndex {

    @Override
    public int findMagicIndex(int[] nums) {
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] == i) {
                return i;
            }
        }
        return -1;
    }

}
