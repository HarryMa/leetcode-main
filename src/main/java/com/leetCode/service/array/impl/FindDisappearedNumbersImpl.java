package com.leetCode.service.array.impl;

import com.leetCode.service.array.FindDisappearedNumbers;

import java.util.*;

public class FindDisappearedNumbersImpl implements FindDisappearedNumbers {

    @Override
    public List<Integer> findDisappearedNumbers(int[] nums) {
        List<Integer> inexistence = new ArrayList<>();
        Set<Integer> numSet = new HashSet<>();
        for (int num : nums) {
            numSet.add(num);
        }
        for (int i = 1; i <= nums.length; i++) {
            if (!numSet.contains(i)) {
                inexistence.add(i);
            }
        }

        return inexistence;
    }

}
