package com.leetCode.service.array.impl;

import com.leetCode.service.array.RemoveDuplicates;

import java.util.HashMap;
import java.util.Map;

public class RemoveDuplicatesImpl implements RemoveDuplicates {

    @Override
    public int removeDuplicates(int[] nums) {
        int len = nums.length;
        if (len == 0) {
            return len;
        }

        int index = 1, number = 1;
        while (number < len) {
            if (nums[number] != nums[number - 1]) {
                nums[index] = nums[number];
                index++;
            }
            number++;
        }

        return index;
    }

    /**
     * 使用了map,空间复杂度>O(1)
     *
     * @param nums 数组
     * @return 数组长度
     */
    public int removeDuplicatesMap(int[] nums) {
        Map<Integer, Integer> map = new HashMap<>();
        int index = 0;
        for (int num : nums) {
            if (!map.containsKey(num)) {
                nums[index] = num;
                map.put(num, num);
                index++;
            }
        }

        return index;
    }

}
