package com.leetCode.service.array.impl;

import com.leetCode.service.array.SearchInsert;

public class SearchInsertImpl implements SearchInsert {

    @Override
    public int searchInsert(int[] nums, int target) {
        for (int i = 0; i < nums.length; i++) {
            if (target <= nums[i]) {
                return i;
            }
        }

        return nums.length;
    }

}
