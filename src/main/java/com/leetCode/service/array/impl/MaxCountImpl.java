package com.leetCode.service.array.impl;

import com.leetCode.service.array.MaxCount;

public class MaxCountImpl implements MaxCount {

    @Override
    public int maxCount(int m, int n, int[][] ops) {
        int x = m;
        int y = n;
        for (int[] op : ops) {
            x = Math.min(x, op[0]);
            y = Math.min(y, op[1]);
        }

        return x * y;
    }

}
