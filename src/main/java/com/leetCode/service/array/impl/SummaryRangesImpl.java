package com.leetCode.service.array.impl;

import com.leetCode.service.array.SummaryRanges;

import java.util.ArrayList;
import java.util.List;

public class SummaryRangesImpl implements SummaryRanges {

    @Override
    public List<String> summaryRanges(int[] nums) {
        List<String> summaryRanges = new ArrayList<>();
        if (nums.length < 1) {
            return summaryRanges;
        }

        int index = 0;
        for (int i = 1; i < nums.length; i++) {
            if (nums[i - 1] != nums[i] - 1) {
                if (index == (i - 1)) {
                    summaryRanges.add(String.valueOf(nums[index]));
                } else {
                    summaryRanges.add(nums[index] + "->" + nums[i - 1]);
                }
                index = i;
            }
        }

        if (index == nums.length - 1) {
            summaryRanges.add(String.valueOf(nums[index]));
        } else {
            summaryRanges.add(nums[index] + "->" + nums[nums.length - 1]);
        }
        return summaryRanges;
    }

}
