package com.leetCode.service.array.impl;

import com.leetCode.service.array.MissingNumber;

public class MissingNumberImpl implements MissingNumber {

    @Override
    public int missingNumber(int[] nums) {
        int length = nums.length;
        int sumLength = 0;
        int sumNums = 0;
        for (int i = 0; i < length; i++) {
            sumLength += (i + 1);
            sumNums += nums[i];
        }

        return sumLength - sumNums;
    }

}
