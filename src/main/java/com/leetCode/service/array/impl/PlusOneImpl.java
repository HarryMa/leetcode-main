package com.leetCode.service.array.impl;

import com.leetCode.service.array.PlusOne;

public class PlusOneImpl implements PlusOne {

    @Override
    public int[] plusOne(int[] digits) {
        int len = digits.length - 1;
        while (len >= 0) {
            int last = digits[len] + 1;
            if (last > 9) {
                digits[len] = 0;
            } else {
                digits[len] = last;
                return digits;
            }
            len--;
        }

        int[] newDigits = new int[digits.length + 1];
        newDigits[0] = 1;
        return  newDigits;
    }

}
