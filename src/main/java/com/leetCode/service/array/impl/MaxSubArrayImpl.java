package com.leetCode.service.array.impl;

import com.leetCode.service.array.MaxSubArray;

public class MaxSubArrayImpl implements MaxSubArray {

    @Override
    public int maxSubArray(int[] nums) {
        int sum = nums[0];
        for (int i = 0; i < nums.length; i++) {
            int crrSum = nums[i];
            sum = Math.max(sum, crrSum);
            for (int j = i + 1; j < nums.length; j++) {
                crrSum = crrSum + nums[j];
                sum = Math.max(sum, crrSum);
                if (crrSum < 0) {
                    break;
                }
            }
        }

        return sum;
    }

}
