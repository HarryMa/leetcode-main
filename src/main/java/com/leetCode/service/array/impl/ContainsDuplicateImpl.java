package com.leetCode.service.array.impl;

import com.leetCode.service.array.ContainsDuplicate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ContainsDuplicateImpl implements ContainsDuplicate {

    @Override
    public boolean containsDuplicate(int[] nums) {
        Map<Integer, Integer> map = new HashMap<>();
        for (int num : nums) {
            if (map.containsKey(num)) {
                return true;
            } else {
                map.put(num, num);
            }
        }

        return false;
    }

    @Override
    public boolean containsNearbyDuplicate(int[] nums, int k) {
        Map<Integer, List<Integer>> map = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            if (map.containsKey(nums[i])) {
                List<Integer> indexList = map.get(nums[i]);
                for (Integer integer : indexList) {
                    if (Math.abs(integer - i) <= k) {
                        return true;
                    }
                }
                indexList.add(i);
            } else {
                List<Integer> indexList = new ArrayList<>();
                indexList.add(i);
                map.put(nums[i], indexList);
            }
        }

        return false;
    }

}
