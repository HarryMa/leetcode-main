package com.leetCode.service.array.impl;

import com.leetCode.service.array.RemoveElement;

public class RemoveElementImpl implements RemoveElement {

    @Override
    public int removeElement(int[] nums, int val) {
        int len = nums.length;
        if (len == 0) {
            return len;
        }

        int index = 0, number = 0;
        while (number < len) {
            if (nums[number] != val) {
                nums[index] = nums[number];
                index++;
            }
            number++;
        }

        return index;
    }

}
