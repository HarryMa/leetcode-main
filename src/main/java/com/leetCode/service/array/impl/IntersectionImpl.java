package com.leetCode.service.array.impl;

import com.leetCode.service.array.Intersection;

import java.util.*;

public class IntersectionImpl implements Intersection {

    @Override
    public int[] intersection(int[] nums1, int[] nums2) {
        List<Integer> intersection = new ArrayList<>();
        Set<Integer> set = new HashSet<>();
        for (int i : nums1) {
            set.add(i);
        }
        for (int i : nums2) {
            if (set.contains(i) && !intersection.contains(i)) {
                intersection.add(i);
            }
        }

        int[] a = new int[intersection.size()];
        for (int i = 0; i < intersection.size(); i++) {
            a[i] = intersection.get(i);
        }
        return a;
    }

    @Override
    public int[] intersect(int[] nums1, int[] nums2) {
        if (nums1.length == 0 || nums2.length == 0) {
            return new int[0];
        }

        List<Integer> intersection = new ArrayList<>();
        Map<Integer, Integer> map = new HashMap<>();
        for (int i : nums1) {
            if (map.containsKey(i)) {
                map.put(i, map.get(i) + 1);
            } else {
                map.put(i, 1);
            }
        }
        for (int i : nums2) {
            if (map.containsKey(i)) {
                int size = map.get(i);
                if (size > 0) {
                    intersection.add(i);
                    map.put(i, size - 1);
                }
            }
        }

        int[] a = new int[intersection.size()];
        for (int i = 0; i < intersection.size(); i++) {
            a[i] = intersection.get(i);
        }
        return a;
    }

}
