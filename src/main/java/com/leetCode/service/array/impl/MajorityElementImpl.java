package com.leetCode.service.array.impl;

import com.leetCode.service.array.MajorityElement;

import java.util.Arrays;

public class MajorityElementImpl implements MajorityElement {

    @Override
    public int majorityElement(int[] nums) {
        Arrays.sort(nums);
        return nums[nums.length / 2];
    }

}
