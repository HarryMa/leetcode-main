package com.leetCode.service.array.impl;

import com.leetCode.service.array.MoveZeroes;

import java.util.ArrayList;
import java.util.List;

public class MoveZeroesImpl implements MoveZeroes {

    @Override
    public void moveZeroes(int[] nums) {
        int amount = 0;
        List<Integer> notZero = new ArrayList<>();
        for (int i : nums) {
            if (i != 0) {
                notZero.add(i);
            } else {
                amount++;
            }
        }

        for (int i = 0; i < nums.length; i++) {
            if (i < (nums.length - amount)) {
                nums[i] = notZero.get(i);
            } else {
                nums[i] = 0;
            }
        }
    }

}
