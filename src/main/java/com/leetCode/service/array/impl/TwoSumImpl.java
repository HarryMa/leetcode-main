package com.leetCode.service.array.impl;

import com.leetCode.service.array.TwoSum;

import java.util.HashMap;
import java.util.Map;

public class TwoSumImpl implements TwoSum {

    @Override
    public int[] twoSum(int[] nums, int target) {
        for (int i = 0; i < nums.length; i++) {
            for (int j = i + 1; j < nums.length; j++) {
                if (nums[i] + nums[j] == target) {
                    return new int[]{i, j};
                }
            }
        }

        return new int[0];
    }

    @Override
    public int[] twoSumTow(int[] numbers, int target) {
        int[] answer = new int[2];
        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < numbers.length; i++) {
            if (map.containsKey(numbers[i])) {
                answer[0] = map.get(numbers[i]);
                answer[1] = i + 1;
            } else {
                map.put(target - numbers[i], i + 1);
            }
        }

        return answer;
    }

    /**
     * 作者：LeetCode-Solution
     * 链接：https://leetcode-cn.com/problems/two-sum/solution/liang-shu-zhi-he-by-leetcode-solution/
     * 来源：力扣（LeetCode）
     * 著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。
     *
     * @param nums nums 2 <= nums.length <= 10^4 -10^9 <= nums[i] <= 10^9
     * @param target -10^9 <= target <= 10^9
     * @return 返回数组
     */
    public int[] twoSumUp(int[] nums, int target) {
        Map<Integer, Integer> hashtable = new HashMap<Integer, Integer>();
        for (int i = 0; i < nums.length; ++i) {
            if (hashtable.containsKey(target - nums[i])) {
                return new int[]{hashtable.get(target - nums[i]), i};
            }
            hashtable.put(nums[i], i);
        }
        return new int[0];
    }

}
