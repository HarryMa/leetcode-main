package com.leetCode.service.array;

public interface RemoveDuplicates {

    /**
     * 删除有序数组中的重复项
     * 给你一个有序数组 nums ，请你 原地 删除重复出现的元素，使每个元素 只出现一次 ，返回删除后数组的新长度。
     * 不要使用额外的数组空间，你必须在 原地 修改输入数组 并在使用 O(1) 额外空间的条件下完成。
     *
     * @param nums 0 <= nums.length <= 3 * 10^4, -10^4 <= nums[i] <= 10^4, nums 已按升序排列
     * @return 数组长度
     */
    int removeDuplicates(int[] nums);

}
