package com.leetCode.service.array;

public interface TwoSum {

    /**
     * 两数之和
     * 给定一个整数数组 nums 和一个整数目标值 target，请你在该数组中找出 和为目标值 target  的那 两个 整数，并返回它们的数组下标。
     *
     * @param nums 2 <= nums.length <= 10^4 -10^9 <= nums[i] <= 10^9
     * @param target -10^9 <= target <= 10^9
     * @return 返回数组
     */
    int[] twoSum(int[] nums, int target);

    /**
     * 两数之和 II - 输入有序数组
     * 给定一个已按照 非递减顺序排列 的整数数组numbers ，请你从数组中找出两个数满足相加之和等于目标数target 。
     * 函数应该以长度为 2 的整数数组的形式返回这两个数的下标值。numbers 的下标 从 1 开始计数 ，所以答案数组应当满足 1 <= answer[0] < answer[1] <= numbers.length 。
     * 你可以假设每个输入 只对应唯一的答案 ，而且你 不可以 重复使用相同的元素。
     *
     * @param numbers 2 <= numbers.length <= 3 * 10^4, -1000 <= numbers[i] <= 1000, numbers 按 非递减顺序 排列
     * @param target -1000 <= target <= 1000
     * @return 返回数组
     */
    int[] twoSumTow(int[] numbers, int target);

}
