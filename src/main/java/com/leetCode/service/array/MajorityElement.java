package com.leetCode.service.array;

public interface MajorityElement {

    /**
     * 多数元素
     * 给定一个大小为 n 的数组，找到其中的多数元素。多数元素是指在数组中出现次数 大于⌊ n/2 ⌋的元素。
     *
     * @param nums 数组是非空的，并且总是存在多数元素。
     * @return 多位数
     */
    int majorityElement(int[] nums);

}
