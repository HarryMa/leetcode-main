package com.leetCode.service.array;

public interface Intersection {

    /**
     * 两个数组的交集
     * 给定两个数组，编写一个函数来计算它们的交集。
     *
     * @param nums1 正整数数组
     * @param nums2 正整数数组
     * @return 交集
     */
    int[] intersection(int[] nums1, int[] nums2);

    /**
     * 两个数组的交集 II
     * 给你两个整数数组nums1 和 nums2 ，请你以数组形式返回两数组的交集。
     * 返回结果中每个元素出现的次数，应与元素在两个数组中都出现的次数一致（如果出现次数不一致，则考虑取较小值）。
     *
     * @param nums1 1 <= nums1.length <= 1000, 0 <= nums1[i] <= 1000
     * @param nums2 1 <= nums2.length <= 1000, 0 <= nums2[i] <= 1000
     * @return 交集
     */
    int[] intersect(int[] nums1, int[] nums2);

}
