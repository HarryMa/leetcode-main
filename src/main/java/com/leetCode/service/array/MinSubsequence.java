package com.leetCode.service.array;

import java.util.List;

public interface MinSubsequence {

    /**
     * 非递增顺序的最小子序列
     * 给你一个数组 nums，请你从中抽取一个子序列，满足该子序列的元素之和 严格 大于未包含在该子序列中的各元素之和。
     *
     * @param nums 1 <= nums.length <= 500, 1 <= nums[i] <= 100
     * @return 符合条件的集合
     */
    List<Integer> minSubsequence(int[] nums);

}
