package com.leetCode.service.array;

public interface MaxCount {

    /**
     * 范围求和 II
     * 给你一个 m x n的矩阵M，初始化时所有的0和一个操作数组op，其中ops[i] = [ai, bi]意味着当所有的0 <= x < ai和0 <= y < bi 时，M[x][y]应该加1。
     *
     * @param m 1 <= m <= 4 * 10^4
     * @param n 1 <= n <= 4 * 10^4
     * @param ops 0 <= ops.length <= 10^4, ops[i].length == 2, 1 <= ai <= m, 1 <= bi <= n
     * @return 计算并返回矩阵中最大整数的个数
     */
    int maxCount(int m, int n, int[][] ops);

}
