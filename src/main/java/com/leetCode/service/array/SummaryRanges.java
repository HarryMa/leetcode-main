package com.leetCode.service.array;

import java.util.List;

public interface SummaryRanges {

    /**
     * 汇总区间
     * 给定一个无重复元素的有序整数数组 nums 。
     * 返回 恰好覆盖数组中所有数字的最小有序区间范围列表。
     * 列表中的每个区间范围 [a,b] 应该按如下格式输出：
     * "a->b" ，如果 a != b
     * "a" ，如果 a == b
     *
     * @param nums 0 <= nums.length <= 20, -2^31 <= nums[i] <= 2^31 - 1, nums 中的所有值都 互不相同, nums 按升序排列
     * @return 返回汇总去捡
     */
    List<String> summaryRanges(int[] nums);

}
