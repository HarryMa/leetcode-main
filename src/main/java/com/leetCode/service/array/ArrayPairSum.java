package com.leetCode.service.array;

public interface ArrayPairSum {

    /**
     * 561 数组拆分 I
     * 给定长度为2n的整数数组nums，你的任务是将这些数分成n对，例如 (a1,b1),(a2,b2),...,(an,bn),使得从1到n的min(ai, bi)总和最大。
     * 返回该 最大总和 。
     *
     * @param nums 1 <= n <= 10^4, nums.length == 2 * n, -10^4 <= nums[i] <= 10^4
     * @return 最大总和值
     */
    int arrayPairSum(int[] nums);

}
