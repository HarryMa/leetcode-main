package com.leetCode.service.array;

public interface SearchInsert {

    /**
     * 搜索插入位置
     * 给定一个排序数组和一个目标值，在数组中找到目标值，并返回其索引。如果目标值不存在于数组中，返回它将会被按顺序插入的位置。
     *
     * @param nums 1 <= nums.length <= 10^4, -10^4 <= nums[i] <= 10^4, nums 为无重复元素的升序排列数组
     * @param target -10^4 <= target <= 10^4
     * @return 插入位置
     */
    int searchInsert(int[] nums, int target);

}
