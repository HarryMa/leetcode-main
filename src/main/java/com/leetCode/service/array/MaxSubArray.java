package com.leetCode.service.array;

public interface MaxSubArray {

    /**
     * 最大子序和
     * 给定一个整数数组 nums ，找到一个具有最大和的连续子数组（子数组最少包含一个元素），返回其最大和。
     *
     * @param nums 1 <= nums.length <= 3 * 10^4, -10^5 <= nums[i] <= 10^5
     * @return 最大和
     */
    int maxSubArray(int[] nums);

}
