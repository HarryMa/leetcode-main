package com.leetCode.service.array;

public interface ContainsDuplicate {

    /**
     * 存在重复元素
     * 给定一个整数数组，判断是否存在重复元素。
     *
     * @param nums 整数数组
     * @return 返回是否存在
     */
    boolean containsDuplicate(int[] nums);

    /**
     * 存在重复元素 II
     * 给定一个整数数组和一个整数k，判断数组中是否存在两个不同的索引i和j，使得nums [i] = nums [j]，并且 i 和 j的差的 绝对值 至多为 k。
     *
     * @param nums 整数数组
     * @param k 整数
     * @return 是否存在
     */
    boolean containsNearbyDuplicate(int[] nums, int k);

}
