package com.leetCode.service.tree.impl;

import com.leetCode.provide.TreeNode;
import com.leetCode.service.tree.GetMinimumDifference;

public class GetMinimumDifferenceImpl implements GetMinimumDifference {

    int pre;
    int ans;

    @Override
    public int getMinimumDifference(TreeNode root) {
        ans = Integer.MAX_VALUE;
        pre = -1;
        dfs(root);
        return ans;
    }

    public void dfs(TreeNode root) {
        if (root == null) {
            return;
        }
        dfs(root.left);
        if (pre != -1) {
            ans = Math.min(ans, root.val - pre);
        }
        pre = root.val;
        dfs(root.right);
    }

    /**
     * 尴尬的看错题目了,写了个相邻两个数的最小差值
     *
     */
    public static int getMinimumNearby(TreeNode root) {
        int left = Integer.MAX_VALUE, right = Integer.MAX_VALUE;
        if (root.left != null) {
            left = Math.min(getMinimumNearby(root.left), Math.abs(root.val - root.left.val));
        }
        if (root.right != null) {
            right = Math.min(getMinimumNearby(root.right), Math.abs(root.val - root.right.val));
        }

        return Math.min(left, right);
    }
}
