package com.leetCode.service.tree.impl;

import com.leetCode.provide.TreeNode;
import com.leetCode.service.tree.SumOfLeftLeaves;

public class SumOfLeftLeavesImpl implements SumOfLeftLeaves {

    @Override
    public int sumOfLeftLeaves(TreeNode root) {
        return root == null ? 0 : sumOfLeft(root);
    }

    public int sumOfLeft(TreeNode root) {
        TreeNode left = root.left;
        TreeNode right = root.right;

        int sum = 0;
        if (left != null) {
            sum += isNextNull(left) ? left.val : sumOfLeft(left);
        }
        if (right != null) {
            sum += isNextNull(right) ? 0 : sumOfLeft(right);
        }

        return sum;
    }

    public boolean isNextNull(TreeNode root) {
        return root.left == null && root.right == null;
    }

}
