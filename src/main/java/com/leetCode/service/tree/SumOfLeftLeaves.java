package com.leetCode.service.tree;

import com.leetCode.provide.TreeNode;

public interface SumOfLeftLeaves {

    /**
     * 左叶子之和
     * 计算给定二叉树的所有左叶子之和。
     *
     * @param root 随机二叉树
     * @return 左叶子之和
     */
    int sumOfLeftLeaves(TreeNode root);

}
