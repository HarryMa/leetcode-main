package com.leetCode.service.tree;

import com.leetCode.provide.TreeNode;

public interface GetMinimumDifference {

    /**
     * 530 二叉搜索树的最小绝对差
     * 给你一个二叉搜索树的根节点 root ，返回 树中任意两不同节点值之间的最小差值 。
     * 差值是一个正数，其数值等于两值之差的绝对值。
     *
     * @param root 树中节点的数目范围是 [2, 10^4], 0 <= Node.val <= 10^5
     * @return 最小差值
     */
    int getMinimumDifference(TreeNode root);

}
