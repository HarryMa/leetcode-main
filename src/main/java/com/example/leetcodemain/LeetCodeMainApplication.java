package com.example.leetcodemain;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LeetCodeMainApplication {

    public static void main(String[] args) {
        SpringApplication.run(LeetCodeMainApplication.class, args);
    }

}
